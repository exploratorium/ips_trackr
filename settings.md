# <*projectName*>_settings.csv

## Description 
There are only two columns and three rows to this csv file.  

This csv file is used to specify the areas_Names_file and  areas_Shape_file files (in the respective cell, in the **v1** column).

## Example

### EXPLO
EXPLO_settings.csv

```
variable,v1
areas_Names_file,EXPLO_galleryNames.csv
areas_Shape_file,EXPLO_galleryShapes.csv
```

### OBS
OBS_settings.csv

```
variable,v1
areas_Names_file,OBS_xbitNames.csv
areas_Shape_file,OBS_galleryShape.csv
```
