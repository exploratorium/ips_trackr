# R code to load the needed functions and packages
# check to make sure all the packages needed are installed
required.packages = c("TraMineR","WeightedCluster", "sp")
new.packages = required.packages[!(required.packages %in% 
                                     installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
require("TraMineR")

# set the working directory
setwd("..")

# load the necessary functions
source("R_scripts/IPS_genFunctions.R")
source("R_scripts/seqFunctions.R")

# "EXPLO" contains scrubbed (and altered) location data from the Exploratorium, 
# as an example.
projectName = "EXPLO"
# set csv_fileList, which lists the IPS data files to process 
csv_fileList = "fileList_02.csv"

# create a dataframe from csv_fileList
csv_df = read.csv(paste(projectName,"/dataLogs/",csv_fileList, sep=""), 
                  header = TRUE, stringsAsFactors = FALSE)
csv_df = csv_df[!(is.na(csv_df$track_id) ) & csv_df$track_id != "",]

# The following function converts everything to seconds.  
# If the time data were collected with another time unit,
#  unitsPerSec will be used to scale the time to seconds.
unitsPerSec = 1000  #ms in a second --- the example device timestamps in ms

# for each IPS data file, find the area each long/lat coordinate was in 
for (i in 1:nrow(csv_df)){
  createFile_areaLocated(projectName,csv_df[i,], unitsPerSec, file_suffix = "proc")
}

# R code to create the SPS sequence data for the 
# two example IPS datafiles found in EXPLO/dataLogs/Test_02

projectName = "EXPLO"
# t_step is the time resolution.In this example, we are interested in
# the location of the visitors tracked every second
t_step = 1 
csv_fileList = "fileList_02.csv"
seq.fn =create_areaSeqCsv(projectName, csv_fileList, t_step, file_suffix= "proc"  )

# R code to read SPS formatted sequence data
projectName = "EXPLO"
t_step = 10
#seq.fn = "galTrack_list_10_seq.csv"
data.seq = get_Seq(projectName, seq.fn)
# create the color palette
cpal(data.seq) =get_kolorVector(data.seq, projectName)

# R code to graph mean time in each gallery

# convert to minutes for graphing
a = seqmeant(data.seq)*t_step/60
max_min = ceiling(max(a))
max_y = (5+ max_min)*60/t_step

# creates the necessary directory
dir.create(file.path(projectName, "results/sequence/pic"),  
           showWarnings = FALSE)

# generate plot
png(file=paste(projectName, 
               "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqmtplot.png", sep=""),width=800, height=600)
seqmtplot(data.seq, ylab = "Mean Time (minutes)",  yaxis=FALSE, 
          xlab = "Gallery", xaxis = F, with.legend=F,  cex.axis = 2, 
          ylim = c(0, max_y ), border = NA)
axis(2, at=seq(from=0, to=max_y , by=60), label = seq(from=0, 
                                                      to=max_y , by=60)/(60/t_step), cex.axis = 1)
legend("topright", alphabet(data.seq), fill=cpal(data.seq),  
       ncol=4, cex = 1)
dev.off()

# R code to generate a density plot of how many visitors 
# are in each gallery for each point in time
data.sps = read.csv(paste(projectName,  
                          "/results/sequence/", seq.fn, sep = ""),  
                    header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(as.matrix(data.sps[,2]), from = "SPS",  
                                 to = "STS", compressed = TRUE), 
                       id = data.sps[,1], missing = NA,  
                       right = "finished")

png(file=paste(projectName, "/", 
               "results/sequence/pic/",gsub(".csv", "",seq.fn), 
               "_seqdplot.png", sep=""),width=800, height=600)
seqdplot(data.fill.seq, xtstep = 900/t_step, 
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", 
                                                  colnames(data.seq)))-1),
         xlab = "Time in Museum ", with.legend = F,
         ylab = "Fraction of Visitors",  xaxis = F,
         cpal =  c(get_kolorVector(data.seq, projectName), 
                   "#FFFFFF"),
         border = NA)
max_time =max (t_step *as.numeric(gsub("[[:punct:]]", "", 
                                    colnames(data.seq))))
axis(1, at=seq(from=0, to=max_time ,    by=t_step*60), 
     label = to_HHMM(seq(from=0,   to=max_time , by=t_step*60)), 
     cex.axis = 1)
legend("topright", alphabet(data.seq), fill=cpal(data.seq),  
       ncol=3, cex = 0.75)
dev.off() 

# R code to reveal where the plurality of visitors are 
# at each point in time
png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(data.fill.seq, xtstep = 900/t_step,
          xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]",
                                                   "", colnames(data.seq)))-1),
          xlab = "Time in Museum ", with.legend = F,
          ylab = "Plurality of Visitors",   xaxis = F,
          cpal =  c(get_kolorVector(data.seq, projectName),
                    "#FFFFFF"),
          border = NA)
max_time =max (t_step *as.numeric(gsub("[[:punct:]]", "", 
                                       colnames(data.fill.seq))))
axis(1, at=seq(from=0, to=max_time ,    by=t_step*60), 
     label = to_HHMM(seq(from=0,   to=max_time , by=t_step*60)), 
     cex.axis = 1)
legend("topright", alphabet(data.seq), fill=cpal(data.seq),  
       ncol=3, cex = 0.75)
dev.off() 

# R code to generate SPS formatted sequence data for 
# normalized total time

# get maximum time for each visit in the data set
dwell = seqlength(data.seq)
max_dwell =max(dwell)  # in t_step units

# create and write normalized sequence data
for (i in 1:nrow(data.sps)){
  # decompose
  a = unlist(strsplit(data.sps[i,2], "-\\("))
  a = gsub("\\(", "", a)
  a = gsub("\\)", "", a)
  b = strsplit(a, ",")
  for (j in 1:length(b)){
    b[[j]][2] = round(as.numeric(b[[j]][2]) *max_dwell/dwell[i])
  }
  # compose
  b = unlist(b)
  gallery = b[seq(1, length(b), 2)]
  gal_dwell = b[seq(2, length(b), 2)]
  data.sps[i,2] = paste(paste("(", gallery , ",", gal_dwell, ")",
                              sep = ""), collapse = "-")
}
write.csv(data.sps,
          paste(projectName,"/results/sequence/","normalized","_",
                gsub(".csv", "", seq.fn),
                ".csv",  sep=""),     
          row.names=FALSE) 

# use the resulting SPS file to generate a density plot
projectName = "EXPLO"
t_step = 10
seqNorm.fn = paste("normalized","_",gsub(".csv", "", seq.fn), ".csv",  sep="")
dataNorm.seq = get_Seq(projectName, seqNorm.fn)
cpal(dataNorm.seq) =get_kolorVector(dataNorm.seq, projectName)
dwell = seqlength(dataNorm.seq)
max_dwell =max(dwell)  # in t_step units

png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seqNorm.fn),
               "_seqdplot.png", sep=""),width=800, height=600)
seqdplot(dataNorm.seq, xtstep = round(max_dwell/5),
         xtlab =  (as.numeric(gsub("[[:punct:]]", "",
                                   colnames(dataNorm.seq)))-1)/(5*round(max_dwell/5)),
         xlab = "Time in Museum (Fraction of total time)", 
         ylab = "Fraction of Visitors", with.legend = F, 
         border = NA)
dev.off()

# use the 'time normalized' SPS file to generate a plot of 
# where the plurality of visitors are
png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(dataNorm.seq, xtstep = round(max_dwell/5),
          xtlab =  (as.numeric(gsub("[[:punct:]]", "",
                                    colnames(dataNorm.seq)))-1)/(5*round(max_dwell/5)),
          xlab = "Time in Museum (Fraction of total time)",
          ylab = "Fraction of Visitors", main = NULL,
          with.legend = F, 
          border = NA)
dev.off()  

# R code to visualize all 103 individual sequences 

# read in the SPS formatted sequence data (not time normalized)
projectName = "EXPLO"
t_step = 10
#seq.fn = "galTrack_list_10_seq.csv"
data.seq = get_Seq(projectName, seq.fn)
cpal(data.seq) =get_kolorVector(data.seq, projectName)

data.sps = read.csv(paste(projectName,  
                          "/results/sequence/", seq.fn, sep = ""),  
                    header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(as.matrix(data.sps[,2]), from = "SPS",  
                                 to = "STS", compressed = TRUE), 
                       id = data.sps[,1], missing = NA,  
                       right = "finished")

# plot and stack each visitor's sequence and sort according to total visit time
png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqIplot.png", sep=""),width=800, height=600)
seqIplot(data.seq,   xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "",
                                                  colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         cex.plot = 1, yaxis = F, sortv =  "from.end",
         cpal =  get_kolorVector(data.seq, projectName))
dev.off() 

# R code to create the pairwise distance (or difference) matrix using Optimal Matching.

# use the conventional indel cost of 1; indel = 1
# use the transition rate as the substitution cost.  This means that the 
# more likely gallery A follows gallery B,
# the less the cost of substituting A for B;sm = "TRATE"
distance.om = seqdist(data.seq, method = "OM", indel = 1, sm = "TRATE")

# R code to define clusters of sequences 

require(WeightedCluster)
# use the ward method in the cluster analysis
clusterward = agnes(distance.om, diss = TRUE, method = "ward")

# generate the dendrogram
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_dendrogram.png", sep=""),width=800, height=600)
plot(clusterward, which.plots = 2, labels = FALSE, main = "")
dev.off() 

# R code to compute cluster membership.  In this example, 
# we assume there are 2 clusters.
cluster_count = 2
clusters = cutree(clusterward, k = cluster_count )
clusters =  paste("Cluster", clusters)

# R code to  plot the average dwell time per gallery  
a = seqmeant(data.seq)*t_step/60
max_min = ceiling(max(a))
max_y = (5+ max_min)*60/t_step
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqmtplot.png", sep=""),width=800, height=600)
seqmtplot(data.seq, group = clusters,ylab = "Mean Time (minutes)",  yaxis=FALSE, 
          xlab = "Gallery", xaxis = F, withlegend=F,  cex.plot = 2, 
          ylim = c(0, max_y ), border = NA)
dev.off() 

# R code to plot the sequences according to cluster membership
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqIplot.png", sep=""),width=800, height=600)
seqIplot(data.seq, group = clusters,  xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         cex.plot = 1.5, yaxis = F, sortv =  "from.end",
         border = NA)
dev.off() 

# R code to  plot density of visitors in each gallery according to time and by cluster.  
# (This is a transversal property)
data.sps = read.csv(paste(projectName, "/results/sequence/", seq.fn, sep = ""), 
                    header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(data.sps[,2], from = "SPS", 
                                 to = "STS", compressed = TRUE), 
                       id = data.sps[,1], missing = NA, right = "finished")

png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqdplot.png", sep=""),
    width=800, height=600)
seqdplot(data.fill.seq, group = clusters,  xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "",
                                                  colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         ylab = "Fraction of Visitors",  cex.plot = 1, 
         cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"), "#FFFFFF"),
         border = NA)
dev.off() 

# R code to  plot the gallery with the plurality of visitors according to time.  
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(data.fill.seq, group = clusters,xtstep = 900/t_step,
          xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", colnames(data.seq)))-1),
          xlab = "Time in Museum ", withlegend = F,
          ylab = "Fraction of Visitors",  cex.plot = 1, 
          cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"), 
                    "#FFFFFF"),
          border = NA)
dev.off()

# R code to see if there is a relationship between cluster membership 
# and type of day data was collected
recruitDay.df = read.csv(paste(projectName,  "/demog_dt.csv", sep = ""), 
                         header = TRUE, stringsAsFactors = FALSE)

# this adds the cluster membership result to information about 
# the day when data was collected
cluster.mem = rep(0, times = nrow(recruitDay.df))
for (i in 1:length(recruitDay.df$track_id ))
  cluster.mem[i] = clusters[recruitDay.df$track_id[i] == rownames(data.seq)]
recruitDay.df = cbind(recruitDay.df, cluster.mem)

#create the contingency table for chi-square  test
contingency.tbl = table(recruitDay.df$evening, recruitDay.df$cluster.mem)
chisq.test(contingency.tbl) 
