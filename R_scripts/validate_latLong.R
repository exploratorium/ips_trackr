source("R_scripts/IPS_genFunctions.R")

# function for creation of an error table comparing devices locations against a human tracker for galleries 
# not based on tessellation 
createErrorTable <- function(projectName,  csv_row,  file_suffix = "proc" ){
  
  fileFolder = paste(projectName, "/dataLogs/",csv_row$subfolder, sep="")
  dir.create(file.path(projectName, "results"),  showWarnings = FALSE)
  dir.create(file.path(projectName, "results/validate"),  showWarnings = FALSE)
  
  # reads in the formatted and processed file named <track_id>_<file_suffix>.csv 
  data = read.csv(paste(fileFolder,"/",csv_row$track_id, "_", file_suffix,".csv", sep=""), 
                  header=T, stringsAsFactors = FALSE)
  
  # gets the names of the areas from the project settings file
  areas_df = read.csv(paste(projectName, "/",
                            get_settingsValue(projectName, "areas_Names_file", "char"),  sep = "") , 
                      header = TRUE, stringsAsFactors = FALSE)
  
  # calculate the true dwell time, device dwell time and time when device was incorrect for each area
  areas_df = areas_df[areas_df$areas != "gallery",]  # gallery is always used to denote an unknown area
  areas = unique(areas_df$areas)
  true.dwell.time = c()
  device.dwell.time = c()  
  truePos =c()  # time device registers as in actual area
  falsePos =c()   # time device registers as  in area but device is actually not in area
  falseNeg =c()  # time device registers as being outside area but device is actually in area
  
  delta_time = c( data$time[2:length(data$time)]-  data$time[1:(length(data$time)-1)], 0 )
  for (area in areas){
    true.dwell.time = c(true.dwell.time, sum(delta_time[data$area_true == area]))
    device.dwell.time = c(device.dwell.time, sum(delta_time[data$area_located == area & data$area_true != "gallery"]))
    truePos =c(truePos, sum(delta_time[data$area_located == area &   data$area_true == area & data$area_true != "gallery"]))  
    falsePos =c(falsePos, sum(delta_time[data$area_located == area &  data$area_true != area & data$area_true != "gallery"]))   
    falseNeg =c(falseNeg, sum(delta_time[data$area_located != area &  data$area_true == area & data$area_true != "gallery"]))  
  }
  
  # calculate the same metrics for groups of areas
  group_col_index = which(grepl("group",colnames(areas_df))  &  !grepl("color",colnames(areas_df)))
  for (gcol in group_col_index ){
    temp_actual_area = data$area_true
    temp_device_area = data$area_located
    for (group in unique(areas_df[areas_df$areas != "gallery",gcol]) ){
      group_members = areas_df$areas[areas_df[, gcol] == group]
      temp_actual_area[data$area_true %in% group_members ] = group
      temp_device_area[data$area_located %in% group_members ] = group
      true.dwell.time = c(true.dwell.time, sum(delta_time[temp_actual_area == group]))
      device.dwell.time = c(device.dwell.time, sum(delta_time[temp_device_area == group ]))
      truePos =c(truePos, sum(delta_time[temp_device_area == group &   temp_actual_area == group ]))  
      falsePos =c(falsePos, sum(delta_time[temp_device_area == group &  temp_actual_area != group  ]))   
      falseNeg =c(falseNeg, sum(delta_time[temp_device_area!= group &  temp_actual_area == group ]))  
      areas = c(areas, group)
    }
  }
  true.dwell.time = to_HHMMSS(true.dwell.time)
  device.dwell.time = to_HHMMSS(device.dwell.time)  
  truePos =to_HHMMSS(truePos)
  falsePos =to_HHMMSS(falsePos)
  falseNeg =to_HHMMSS(falseNeg)
  df = data.frame(areas, true.dwell.time, device.dwell.time, truePos, falsePos , falseNeg )
  
  write.csv( df,paste(projectName,"/results/validate/",csv_row$track_id, "_", 
                      file_suffix,"_errorTable.csv",sep=""),  row.names=FALSE)
}
# not based on tessellation 
createErrorTableCluster <- function(projectName, csv_fileList, csv_row,  file_suffix = "proc" ){
  
  fileFolder = paste(projectName, "/dataLogs/",csv_row$subfolder, sep="")
  dir.create(file.path(projectName, "results"),  showWarnings = FALSE)
  dir.create(file.path(projectName, "results/validate"),  showWarnings = FALSE)
  
  # reads in the formatted and processed file named <track_id>_<file_suffix>.csv 
  data = read.csv(paste(fileFolder,"/",csv_row$track_id, "_", file_suffix,".csv", sep=""), 
                  header=T, stringsAsFactors = FALSE)
  
  # gets the names of the areas from the project settings file
  areas_df = read.csv(paste(projectName, "/",
                            get_settingsValue(projectName, "areas_Names_file", "char"),  sep = "") , 
                      header = TRUE, stringsAsFactors = FALSE)
  
  # get the centroids file to determine neighbors
  centroid.df = read.csv(paste(projectName,"/",gsub(".csv", "", csv_fileList), "_centroids.csv", sep=""), 
                         header=T, stringsAsFactors = FALSE)
  
  # calculate the true dwell time, device dwell time and time when device was incorrect for each area
  areas_df = areas_df[areas_df$areas != "gallery",]  # gallery is always used to denote an unknown area
  areas = unique(areas_df$areas)
  
  inCluster.time =c()  # time device registers as in  area and point falls in cluster
  outCluster.time =c()   # time device registers as  in area but points falls outside of cluster
  
  delta_time = c( data$time[2:length(data$time)]-  data$time[1:(length(data$time)-1)], 0 )
  for (area in areas){
    neighbors = unlist(strsplit(centroid.df[centroid.df== area,]$adjacentXbits, "-"))
    neighbors = c(area, neighbors[neighbors != ""])
    
    inCluster.time = c(inCluster.time, sum(delta_time[data$area_located == area & data$area_true %in% neighbors]))
    outCluster.time = c(outCluster.time, sum(delta_time[data$area_located == area & !(data$area_true %in% neighbors)]))
    
  }
  
  # calculate the same metrics for groups of areas
  inCluster.time = to_HHMMSS(inCluster.time)
  outCluster.time = to_HHMMSS(outCluster.time)  
  df = data.frame(areas, inCluster.time, outCluster.time  )
  
  write.csv( df,paste(projectName,"/results/validate/",csv_row$track_id, "_", 
                      file_suffix,"_clusterErrTable.csv",sep=""),  row.names=FALSE)
}


plot_humanVsDevice <- function(projectName, csv_fileList, csv_df , t_step ,file_suffix= "proc",  
                               cluster_p = FALSE){
  require(TraMineR)
  
  dir.create(file.path(projectName, "results"),  showWarnings = FALSE)
  dir.create(file.path(projectName, "results/pic"),  showWarnings = FALSE)
  
  areas_file = get_settingsValue( projectName, "areas_Names_file", "char")
  areaList = read.csv(paste(projectName, "/",areas_file, sep = "") , header = TRUE, 
                      stringsAsFactor = FALSE,  sep = ",")
  
  for(i in which(grepl("color",colnames(areaList)))){
    legend_item = c()
    
    # to do one track at a time
    for (k in 1:nrow(csv_df)){
      fileFolder = paste(projectName, "/dataLogs/",csv_df$subfolder[k], sep="")
      
      data = read.csv(paste(fileFolder,"/",csv_df$track_id[k] ,"_", file_suffix,".csv",sep="") , 
                      header = TRUE, stringsAsFactor = FALSE, sep = ",")
      
      # use the human track log in case there are large chunks of missing time in the device log
      humanTrack_df = getFormat_humanData(csv_df[k,])
      
      timeticks = seq(floor(min(data$time)),floor(max(data$time)), t_step)
      
      # find closest time for human and device
      vector_reading = c()
      vector_true = c()
      for (j in timeticks){
        vector_reading = c(vector_reading, data$area_located[which.min(abs(data$time - j))])
        vector_true = c(vector_true, humanTrack_df$Enters.into[max(which(humanTrack_df$Watch.time< j))])
      }
      
      notFoundAreas = setdiff(unique(vector_true), areaList[,i-1])
      # set all areas that were not found in the project's area file to gallery
      vector_true[which(!(vector_true %in%  areaList[, 1])) ] = "gallery"        
      
      if(i-1 > 1){  # taking care of categories
        for(j in 1:length(areaList$areas)){
          if(areaList$areas[j] != "gallery"){
            vector_true[vector_true == areaList$areas[j]] = areaList[,i-1][j]
            vector_reading [vector_reading == areaList$areas[j]] = areaList[,i-1][j]
          }
        }
        humanVsDevice.df = generate_HVD_rle(vector_true, vector_reading, c("human", "alg"))
        humanVsDevice.seq = generateHVD_seq(humanVsDevice.df, data, t_step)
      }
      else {  # taking care of areas          
        # generate log of which areas were not found in areas_file
        if(length(notFoundAreas)>0){
          cat('\n', paste(Sys.Date(), format(as.POSIXct(Sys.time()) , "%H:%M:%S")),
              colnames(areaList)[i-1], "  ", as.character(csv_df$track_id[k]), 
              file=paste(projectName,"/results/missing_from_area_file.txt", sep = ""),append=TRUE)
          cat("  ", notFoundAreas, sep ="\n",
              file=paste(projectName,"/results/missing_from_area_file.txt", sep = ""),append=TRUE)
        }
        if(!cluster_p) humanVsDevice.df =generate_HVD_rle(vector_true, 
                                                          vector_reading,
                                                          c("human", "device"))
        else humanVsDevice.df = generate_HVD_Cluster_rle(projectName, 
                                                         csv_fileList,   vector_true, 
                                                         vector_reading,
                                                         c("human", "device"))
        humanVsDevice.seq = generateHVD_seq(humanVsDevice.df, data, t_step)
      }
      
      ind = which(alphabet(humanVsDevice.seq) != "agree" & 
                    alphabet(humanVsDevice.seq) != "disagree" &
                    alphabet(humanVsDevice.seq) != "unk" &
                    alphabet(humanVsDevice.seq) != "clusterMember" )
      kpal =get_kolorVector(humanVsDevice.seq, projectName, i)         
      
      if(k %% 3 == 1){
        if(!cluster_p)
        png(file=paste(projectName,"/results/validate/", colnames(areaList)[i-1], "_Set", ceiling(k/3),
                       "_compare.png", sep=""),width=900, height=800)
        else 
          png(file=paste(projectName,"/results/validate/", colnames(areaList)[i-1], "_ClusterSet", ceiling(k/3),
                         "_compare.png", sep=""),width=900, height=800)
        par(mar=c(2, 5, 2, 2) ,mfrow = c( 2, 2))
      }
      
      legend_item =  union(union(unique(vector_true),unique(vector_reading)),legend_item)
      legend_item = unname(unlist(sapply(legend_item, function(x) {
        if( !(x %in% notFoundAreas))  x
        else NULL
      },  simplify = TRUE)))
      time_axis = max(as.numeric(gsub("[[:punct:]]", "", colnames(humanVsDevice.seq))))
      axis_tick = 15*60
      if(time_axis/4 < 15*60)  axis_tick = 5*60
      if(time_axis/4 < 5*60)  axis_tick = 1*60
      
      seqplot(humanVsDevice.seq[1:3,], border = NA, type = "i",
              title = csv_df$track_id[k],
              ytlab = c("human", "device", "diff"), 
              withlegend=FALSE,group=NULL,
              xtstep = axis_tick, 
              cpal = kpal,
              xtlab = to_HHMM (as.numeric(gsub("[[:punct:]]", "", colnames(humanVsDevice.seq)))*t_step),
              cex.plot = 1.2)
      
      # formatting legend
      if(k %% 3 == 0 | k == nrow(csv_df)){
        legend_kol = unname(sapply(legend_item, function(x) {
          if( x %in% notFoundAreas | x == "gallery") "white"
          else areaList[,i][[min(which(areaList[,i-1]==x))]]
        },  simplify = TRUE))
        plot(1, type = "n", axes=FALSE, xlab="", ylab="")
        if(!cluster_p | i !=2 ){
          legend_elements = c("agree", "disagree","", legend_item)
          bgKolor= c("black", "red","white", legend_kol)
          fKolor=c("black", "black","white", rep("black", times = length(legend_kol )))
        }
        else {
          legend_elements = c("agree", "disagree","cluster member", "", legend_item)
          bgKolor= c("black", "red","darkgrey", "white", legend_kol)
          fKolor=c("black", "black","black", "white", rep("black", times = length(legend_kol )))
        }
        
        col_count = ceiling((4+length(legend_elements))/20)
        ptSize = 2.15 
        if(col_count>2){
          #### cut extra length from vectors for legend
          extras_ind = length(legend_elements) - length(legend_item)
          legend_elements = legend_elements[1:extras_ind]
          bgKolor = bgKolor[1:extras_ind]
          fKolor = fKolor[1:extras_ind]
        }
        
        legend("bottomleft",inset = 0, bty = "n",
               legend = legend_elements, 
               pch = 22,ncol = min(2, col_count),
               pt.bg=bgKolor,    col=fKolor,  
               lty = 0, pt.cex = ptSize, cex = ptSize-.75)
        legend_item = c()
        dev.off()
      }
    }  # end track loop
  }
}
   
# function for calculating Cohen's kappa for device and human tracker area identification
computeKappaHumanDevice <-  function(projectName, csv_fileList,csv_row, file_suffix= "proc"){
  require(irr)
  areas_file = get_settingsValue( projectName, "areas_Names_file", "char")
  areaList = read.csv(paste(projectName, "/",areas_file, sep = "") , header = TRUE, 
                      stringsAsFactor = FALSE,  sep = ",")
  
  dir.create(file.path(projectName, "results"),  showWarnings = FALSE)
  dir.create(file.path(projectName, "results/validate/"),  showWarnings = FALSE)
  sink(paste(projectName,"/results/validate/",gsub(".csv", "", csv_fileList),
             "_kappaIndDevice.txt", sep = ""), append=TRUE, split=TRUE)
  
  fname = paste(projectName,"/dataLogs/", csv_row$subfolder,"/",csv_row$track_id ,"_", file_suffix, ".csv",sep="")   
  cat('\n',"___________________________", '\n',fname, '\n')
  data = read.csv(fname, header = TRUE, stringsAsFactors = FALSE)
  humanTrack_df = getFormat_humanData(csv_row)
  
  # create a common timeline
  t_step = 2*floor(median(data$time[2:nrow(data)] - data$time[1:(nrow(data)-1)]))
  timeticks = seq(floor(min(data$time)),floor(max(data$time)), t_step)
  cat("t_step (based on twice device sampling freq) =", t_step, '\n')
  
  # find closest time for human and device
  vector_reading = c()
  vector_true = c()
  for (j in timeticks){
    vector_reading = c(vector_reading, data$area_located[which.min(abs(data$time - j))])
    vector_true = c(vector_true, data$area_true[which.min(abs(data$time - j))])
  }
  
  vector_true[which(!(vector_true %in%  areaList$areas)) ] = "gallery" 
  # if the human lost the track, remove those data points from the calculation      
  vector_reading = vector_reading[which( vector_true != "gallery")]
  vector_true = vector_true[which( vector_true != "gallery")]
  
  # calculate kappa
  print(kappa2(cbind(vector_reading,vector_true)))
  sink.reset()
}

# function to generate maps showing where the device location does not correspond with the human tracker's location

plotErrorMap_GMap =  function(projectName, csv_fileList, csv_row, map, file_suffix= "proc",  areaShapes = NULL,
                               cluster_p = FALSE){
  
  fileFolder = paste(projectName, "/dataLogs/",csv_row$subfolder, sep="")
  # reads in the formatted and processed file named <track_id>_<file_suffix>.csv 
  data = read.csv(paste(fileFolder,"/",csv_row$track_id, "_", file_suffix,".csv", sep=""), 
                  header=T, stringsAsFactors = FALSE)
   
  # process data differently depending on whether of not we are allowing cluster membership
  if(!cluster_p)
  data = data[data$area_located != data$area_true & data$area_true != "gallery",]
  else {
    # read in the tessellation file <projectName>/<csv_fileList>_centroids.csv if cluster validataion
    centroid.df = read.csv(paste(projectName,"/",gsub(".csv", "", csv_fileList), "_centroids.csv", sep=""), 
                           header=T, stringsAsFactors = FALSE)
    data = data[data$area_located != "gallery" & data$area_true != "gallery",]
    for (area in unique(data$area_located)){
      neighbors = unlist(strsplit(centroid.df[centroid.df== area,]$adjacentXbits, "-"))
      neighbors = c(area, neighbors[neighbors != ""])
      # get only the data that lie outside area and neighbor
      data = data[ data$area_true != area | (!(data$area_located %in% neighbors) & data$area_true == area) ,]
    }
  }
  
  if(is.null(areaShapes)){
    areas_file = get_settingsValue( projectName, "areas_Shape_file", "char")
    areaShapes = read.csv(paste(projectName, "/",areas_file, sep = "") , header = TRUE, 
                          stringsAsFactor = FALSE,  sep = ",")
  }
  areas_file = get_settingsValue( projectName, "areas_Names_file", "char")
  areaColors = read.csv(paste(projectName, "/",areas_file, sep = "") , header = TRUE, 
                        stringsAsFactor = FALSE,  sep = ",")
  
  kolor = rep("white", times = nrow(data))
  for (j in unique(areaColors$area)){
    kolor[j == data$area_true] = areaColors$color[j == areaColors$areas]
  }
  kolor_perimeter = rep("white", times = nrow(data))
  for (j in unique(areaColors$area)){
    kolor_perimeter[j == data$area_located] = areaColors$color[j == areaColors$areas]
  }
  
  if(!cluster_p)
    png(file=paste(projectName,"/results/validate/", csv_row$track_id,"_",
                 file_suffix, "_errorMap.png", sep=""),width=800, height=600)
  else 
    png(file=paste(projectName,"/results/validate/", csv_row$track_id,"_",
                   file_suffix, "_Cluster_errorMap.png", sep=""),width=800, height=600)
  PlotOnStaticMap(map,
                  lat = data$latitude,
                  lon = data$longitude,  
                  col = kolor_perimeter, bg = kolor,
                  pch = 21, 
                  add=FALSE)
  if(cluster_p){
    for (j in unique(areaShapes$area)){
      PlotOnStaticMap(map,
                      lat = centroid.df$centroid_y[which(centroid.df$xbit ==j)],
                      lon = centroid.df$centroid_x[which(centroid.df$xbit ==j)],  
                      FUN = points,  pch = 8, lwd = 1.5,
                      col = areaColors$color[areaColors$areas == j],
                      add=TRUE)
    }
  }
  for (j in unique(areaShapes$area)){
    PlotOnStaticMap(map,
                    lat = areaShapes$latitude[which(areaShapes$area ==j)],
                    lon = areaShapes$longitude[which(areaShapes$area ==j)],  
                    FUN = lines, lwd = 1, lty = 1,  
                    col = areaColors$color[areaColors$areas == j],
                    add=TRUE)
  }
  if(cluster_p){
    for (j in unique(areaShapes$area)){
      TextOnStaticMap(map,
                      lat = centroid.df$centroid_y[which(centroid.df$xbit ==j)],
                      lon = centroid.df$centroid_x[which(centroid.df$xbit ==j)], labels=j, 
                      cex = .5,
                      col = areaColors$color[areaColors$areas == j],
                      add=TRUE)
    }
  }
  
  dev.off()
}

# _________________________________________
# supporting functions

# gets the  human tracking file and format it appropriately
getFormat_humanData = function(csv_row){
  
  humanTrack_file =paste(projectName, "/dataLogs/", csv_row$subfolder,"/",gsub(".csv", "", csv_row$human.datafile),".csv", sep="")
  humanTrack_df = read.csv(humanTrack_file, header = TRUE, stringsAsFactors = FALSE)
  #convert humanTrack to seconds and then 0 the result to beginning of track
  sec_humanTrack = as.character(humanTrack_df$Watch.time)
  sec_humanTrack = sapply(strsplit(sec_humanTrack,":"),
                          function(x) {
                            x <- as.numeric(x)
                            x[1]*60*60+x[2]*60+x[3]
                          })
  humanTrack_df$Watch.time = sec_humanTrack - sec_humanTrack[1]
  return (humanTrack_df)
}

# formatting functions for plotting discrenpacy bar charts
generate_HVD_rle = function(vector_true, vector_reading, cnames = c("true_position", "measured_position")){
  compare.df = c()
  vector_true = as.character(vector_true)
  vector_reading = as.character(vector_reading)
  diff = vector_true
  
  diff[which(vector_true ==  vector_reading) ] = "agree"
  diff[which(vector_true !=  vector_reading & vector_true != "gallery")] = "disagree"
  diff[which( vector_true == "gallery")] = "unk"
  
  reading.rle = rle(vector_reading)
  true.rle = rle(vector_true)
  diff.rle = rle(diff)
  compare.df = rbind( true.rle, reading.rle, diff.rle)
  rownames(compare.df) = c(cnames, "diff")
  
  return(compare.df )
}

generate_HVD_Cluster_rle = function(projectName, csv_fileList,
                                    vector_true, vector_reading, cnames = c("true_position", "measured_position")){
  compare.df = c()
  vector_true = as.character(vector_true)
  vector_reading = as.character(vector_reading)
  diff = vector_true
  
  diff[which(vector_true ==  vector_reading) ] = "agree"
  diff[which(vector_true !=  vector_reading &
               vector_true != "gallery")] = "disagree"
  diff[which( vector_true == "gallery")] = "unk"
  
  # look up centroid.csv file
  centroid_file =paste(projectName,"/", gsub(".csv", "",csv_fileList), "_centroids.csv", sep = "")
  centroids.df  = read.csv(centroid_file, header = TRUE, stringsAsFactors = FALSE)
  clusterList = data.frame(centroids.df$xbit, centroids.df$adjacentXbits)
  colnames(clusterList)= c("xbit", "cluster")
  clusterList$xbit = as.character(clusterList$xbit)
  clusterList$cluster = as.character(clusterList$cluster)
  xbits = clusterList$xbit
  
  for (j in 1:length(xbits)){   
    xbit = xbits[j]
    cluster = clusterList$cluster[clusterList$xbit == xbit]
    diff[which( vector_true== xbit &
                  unname(sapply(vector_reading, function(x) grepl (paste("-",x, "-", sep = ""), cluster))))] = 
      "clusterMember"
  }
  reading.rle = rle(vector_reading)
  true.rle = rle(vector_true)
  
  diff.rle = rle(diff)
  compare.df = rbind( true.rle, reading.rle, diff.rle)
  rownames(compare.df) = c(cnames, "diff")
  
  return(compare.df )
}

generateHVD_seq = function(humanVsDevice.df, df, t_step){  
  data.sps = ""
  timeticks = seq(floor(min(df$time)),floor(max(df$time)), t_step)
  for (j in 1:floor(nrow(humanVsDevice.df)/3)){
    for (k in 1:3){
      a.sps = ""
      running.count = 0
      index = (j-1)*3 + k
      
      for (i in 1:length(humanVsDevice.df[index,]$values)){
        if(running.count == 0 ) last_time = timeticks[1]
        else last_time = timeticks[running.count*t_step]  
        sec = round(timeticks[humanVsDevice.df[index,]$lengths[i]+running.count*t_step]-last_time)
        if(sec <0 ) sec = 0
        
        sec = round(sec/t_step)
        a.sps = paste(a.sps,"(",humanVsDevice.df[index,]$values[i],",", sec,")",sep="")
        a.sps = gsub("\\)\\(", "\\) - \\(", a.sps)
        a.sps = gsub(" ", "", a.sps)
        running.count = humanVsDevice.df[index,]$lengths[i]/t_step +running.count
      }    
      data.sps = rbind(data.sps,a.sps)
    }
  }
  data.sps = data.sps[-1,]
  
  hvd_list =  c( paste(df$track_id[1], "_human", sep = ""),
                 paste(df$track_id[1], "_alg", sep = ""), 
                 paste(df$track_id[1], "_diff", sep = ""))
  
  a.seq = seqdef(seqformat(data.sps, from = "SPS", to = "STS", compressed = TRUE), 
                 id = hvd_list,
                 missing.color="white")
  return(a.seq)
}