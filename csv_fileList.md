# <*csv_fileList*>.csv

This csv file is used to specify the IPS device data file to be processed and any associated human tracking files needed for ground truth testing. 

## Column Description 
This is a csv file with seven columns.  

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|device|  device name| text |  |
|subfolder |  subfolder under <*projectName*>/dataLogs where the datafiles are located | text |   |
|device.datafile | datafile of lat/long and time collected by the tracking device | text |   |
|device.start.time | when the device started collecting data | float | time units should be the same as that in device.datafile
|track_id | name of the track; a unique identifier assigned by the user |text |   |
| human.watch.start.time | the time on the human stopwatch when the device was turned on | hh:mm:ss | optional; required only for ground-truth testing  |
|human.datafile | timing and tracking datafile collected by the human tracker | text | optional; required only for ground-truth testing |

## Example
### EXPLO
dataLogs/fileList_01.csv

```
device,subfolder,device.datafile,device.start.time,track_id,human.watch.start.time,human.datafile
D2,Test_02,LocData_V1.csv, 1393910477756,V1,0:00:00,humanTrackerLog_V1.csv
D5,Test_02,LocData_V6.csv, 1393968098137,V6,0:00:00,humanTrackerLog_V6.csv
D2,Test_01,LocData_D2.csv, 1397087249704,Test01_D2,0:00:01,humanTrackerLog.csv
D1,Test_01,LocData_D1.csv, 1398301276602,Test01_D1,0:06:51,humanTrackerLog.csv
```


### OBS
dataLogs/fileList_Xbit.csv
```
device,subfolder,device.datafile,device.start.time,track_id,human.watch.start.time,human.datafile
S1,Test_03,LocData_S1,1426712619920,Test3_S1,0:02:45,humanTrackerLog.csv
I1,Test_03,LocData_I1,1426712454050,Test3_I1,0:00:00,humanTrackerLog.csv
```