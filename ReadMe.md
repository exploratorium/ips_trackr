# What is trackR?
TrackR is a collection of example scripts and supporting functions written to facilitate 1) the validation and 2) the analysis of timing and tracking data collected for research and evaluation in the visitor studies field.  Timing and tracking data are collected in the museum field to understand where visitors go and how long they stay.   Traditionally, timing and tracking data have been collected by human trackers who would note which exhibit, exhibition or gallery a visitor being tracked is at and the dwell time at that exhibit or area.  From 2013 to 2015, the Exploratorium, with funding from the National Science Foundation (EAGER 134666[]()4), experimented with a prototype WiFi-based indoor positioning system to determine how such a system could be used to automate the collection of timing and tracking data.  This prototype collected timestamped longitude  and latitude coordinates with a mobile application and phone, which a person, being tracked, would wear.  A large part of this project, Indoor Positioning System (IPS), focused on developing methods to

1. validate the data collected by the prototype system at three levels of resolution: gallery, cluster, and exhibit and 

2. analyze the resulting data collected.  

The scripts and functions in this repository were developed as part of this project.  Although they were defined while working with a WiFi-based indoor positioning system, many of the scripts and functions can be adapted and used to process data collected by other timing and tracking systems, including Bluetooth Low Energy (BLE) beacon proximity systems and human trackers noting exhibit stops and dwell times.

The examples in this repository contain highly scrubbed, altered and invented data that are for illustrative purposes only.  No raw data are provided.

## Validation scripts (Ground-truth testing)
Ground-truth testing is the process of gathering data in the field to validate/ dispute data collected by the technological system under investigation.  This process is critical to determining the limitations of the data collection system. A collection of R scripts and functions were written to support ground-truth testing protocols defined to assess the location data collected by an indoor positioning system at three levels of resolution: 

1. gallery-level [protocol](validate/protocol_validate_galleryLoc.md) and [script](validate/script_validate_galleryLoc.md)  

2. exhibit-level [protocol](validate/protocol_validate_xbitLoc.md) and [script](validate/script_validate_xbitLoc.md) 

3. cluster-level [protocol](validate/protocol_validate_clusterLoc.md)  and [script](validate/script_validate_clusterLoc.md)

## Analysis scripts
The data collected by an indoor positioning system can be analyzed in multiple ways.  This repository provides example scripts and supporting functions that step through two types of analyses:

1. [sequence analysis](analysis/example_sequenceAnalysis.md).  Using the R package, [TraMineR](https://cran.r-project.org/web/packages/TraMineR/index.html), this example looks at the timing and tracking data collected as an ordered sequence of galleries visited by the person tracked.  

2. [spatial analysis](analysis/example_spatialAnalysis.md).  This script steps through the generation of visualizations of where visitors are.

These examples focus on exploratory data analyses and serve as templates which can be adapted by the user for his/her own data.

## Running the R Scripts
We recommend using RStudio, an integrated development environment (IDE) for R, to run the scripts.  There is a free, open-source version of RStudio available.  R itself is free.  Installation instructions for RStudio and R can be found on the [RStudio] and the [R Project] site, respectively.

[RStudio]: <https://www.rstudio.com/products/rstudio/download/>
[R Project]: <https://www.r-project.org/>

## License
Unless stated otherwise, all content is licensed under the [Creative Commons Attribution License](http://creativecommons.org/licenses/by/3.0/) and code licensed under the [MIT License](http://creativecommons.org/licenses/MIT/).
Copyright (c) 2016 Joyce Ma, Exploratorium

## Acknowledgement
This material is based upon work supported by the National Science Foundation under Grant No. 134666[]()4. Any opinions, findings, and conclusions or recommendations expressed in this material are those of the authors and do not necessarily reflect the views of the National Science Foundation.

![alt text](nsf1.jpg "NSF Logo")