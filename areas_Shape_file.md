# <*areas_Shape_file*>.csv

This csv file is used to specify each area's shape.  Each area is defined as a closed polygon using an ordered sequence of its vertices (as latitude and longitude coordinates, traversing the area's shape clockwise or counterclockwise). 

## Column Description
This is a csv file with four columns. 

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|area| the names of the areas (e.g. galleries, clusters, exhibits) of interest | text | 
|longitude | longitude of the area's vertex | float | the first value must be the same as the last value for an area|
|latitude | latitude of the area's vertex | float | the first value must be the same as the last value for an area|
|floor | floor the area is on | integer | optional if there is only one floor  |


## Example
### EXPLO
EXPLO_galleryShapes.csv

```
area,longitude,latitude,floor
Entry,-122.398879,37.80101824,1
Entry,-122.3986395,37.80120818,1
Entry,-122.3979179,37.80158568,1
Entry,-122.3978875,37.8015382,1
Entry,-122.3979148,37.80151683,1
Entry,-122.3978966,37.80149783,1
Entry,-122.3980028,37.8014456,1
Entry,-122.3980179,37.8014551,1
Entry,-122.3984757,37.80120818,1
Entry,-122.3984757,37.80112033,1
Entry,-122.3983484,37.80097076,1
Entry,-122.3986728,37.80079032,1
Entry,-122.398879,37.80101824,1
...

Obs,-122.396987,37.80238817,2
Obs,-122.3967475,37.80251163,2
Obs,-122.3966202,37.8023763,2
Obs,-122.3966444,37.80236205,2
Obs,-122.3965262,37.80223147,2
Obs,-122.3966596,37.80216974,2
Obs,-122.3967627,37.80228608,2
Obs,-122.3968506,37.80223859,2
Obs,-122.396987,37.80238817,2
SE Apron,-122.396696,37.80167115,1
SE Apron,-122.3966414,37.80160705,1
SE Apron,-122.396229,37.8018421,1
SE Apron,-122.3962321,37.80193944,1
SE Apron,-122.396696,37.80167115,1
```

### OBS
OBS_galleryShape.csv

```
area,latitude,longitude,floor
Obs,37.80237938,-122.3969882,2
Obs,37.80222732,-122.3968514,2
Obs,37.80236508,-122.3966157,2
Obs,37.80251078,-122.3967484,2
Obs,37.80237938,-122.3969882,2
```
