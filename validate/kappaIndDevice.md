# <*csv_fileList*>\_kappaIndDevice.txt

This txt file contains the results of running the Cohen’s kappa calculations (R package irr, kappa2 function), in which the device tracker and the human tracker are the two coders.  The calculation does not consider the times when the human tracker has lost his/her target or the setup time before or after the human tracker has started or ended tracking.  (These times should be noted with the area value, 'gallery', in the human tracker's data sheet). 


## Example

Examples can be found in: 

+ [EXPLO/results/validate/fileList_01_kappaIndDevice.txt](../EXPLO/results/validate/fileList_01_kappaIndDevice.txt)

+ [OBS/results/validate/fileList_Xbit_kappaIndDevice.txt](../OBS/results/validate/fileList_Xbit_kappaIndDevice.txt)
