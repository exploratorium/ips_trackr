## Gallery-level Validation Protocol

### A. Collect ground truth data

1. Define the boundaries of each gallery.
2. Synchronize the mobile tracking devices’ time with the human tester’s watch.  This establishes the common time that will be critical to aligning the two datasets for later comparison.
3. Turn on the devices’ tracking application.
4. Human tester wears all the devices.
5. Human tester walks a prescribed path that crosses every boundary, noting the time when s/he enters a gallery, with paper and pencil.
6. Download the data from the mobile devices.


![alt text](galleryProtocol_F1.png "Collecting Ground Truth Data")

***Human tester walks a prescribed path crossing every gallery boundary, while noting (w/ paper and pencil ) which gallery s/he enters and when.***

### B.Process and assess the data 
The [example script](script_validate_galleryLoc.md)  calls functions that process and help assess the data:

1.	Finds the area each long/lat data point is in. 
2.	Runs any project-specific data smoothing or cleaning algorithms.
3.	Aligns those data with data collected by a  human tracker.
4.	Calculates the dwell time discrepancies between the human and device location data.
5.	Computes the Cohen's kappa as an interrater reliability metric for the data collected.


![alt text](galleryProtocol_F2.png "Processing Ground Truth Data")

***Align the data collected by the human tracker with the device tracker to determine in which gallery  a location data point falls.***

