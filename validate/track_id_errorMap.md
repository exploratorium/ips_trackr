# <*track_id*>_errorMap.png

This png file plots  the location discrepancy between the human and device tracker.  A png is created for each device tracker datafile specified in <*projectName*>/dataLogs/[<*csv_fileList*>.csv]  

[<*csv_fileList*>.csv]: </csv_fileList.md>
## Example
### EXPLO
results/validate/Test01_D1_proc_errorMap.png

The EXPLO example uses scrubbed data and is for illustrative purposes only.  

![Example plotting location discrepancies between device versus human tracker](errorMap_EXPLO.png)

*A dot indicates a data point for which the human and the device trackers disagreed.  The position of the point is the longitude and latitude coordinates as logged by the device.  The fill of a point is the color of the area according to the **human** tracker.  The perimeter color indicates the area according to the **device** tracker.*

### OBS
results/validate/Test3_S1_proc_errorMap.png

The OBS example uses simulated and invented data and is for illustrative purposes only.  

![Example plotting location discrepancies between device versus human tracker](errorMap_OBS.png)

*A dot indicates a data point for which the human and the device trackers disagreed.  The position of the point is the longitude and latitude coordinates as logged by the device.  The fill of a point is the color of the area according to the **human** tracker.  The perimeter color indicates the area according to the **device** tracker.*