# <*track_id*>_clusterErrTable.csv

This csv file contains the computed time when  the device tracker located the human/object that was being tracked as being within or outside of an exhibit cluster.  

## Column Description 


| column name | description | format |notes |
|----------|---------|-------------|-------------|
|areas  |  exhibit name| text | exhibits listed in [<*csv_fileList*>_centroids.csv] |
|inCluster.time | time when the device registered as being at the exhibit and the human tracker was either at the exhibit itself or at one of its adjacent neighbors | hh:mm:ss |  |
|outCluster.time | time when the device registered as being at the exhibit but the human tracker was **NOT**  at either the exhibit itself or at one of its adjacent neighbors|  hh:mm:ss |  |



## Example
###OBS
results/validate/Test3_S1_proc_errorClusterTable.csv

```
"areas","inCluster.time","outCluster.time"
"City Viewer","00:00:51","00:01:53"
"Pic Place City","00:01:04","00:00:17"
"Shift Shore","00:01:21","00:01:23"
"Sighter","00:01:42","00:14:31"
...
"Tidal Ribbon","00:01:32","00:00:23"
"Mud Slide","00:01:00","00:00:45"
"Sed Core","00:02:29","00:00:47"
```

[<*csv_fileList*>_centroids.csv]: <centroids.md>