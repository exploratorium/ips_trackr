# area_Set\_compare.png

This png file visualizes the time alignment of the sequence of locations the human versus the device tracker identified, and shows the discrepancies. A visualization is produced for each device.datafile specified in <*projectName*>/dataLogs/[<*csv_fileList*>.csv]  

[<*csv_fileList*>.csv]: </csv_fileList.md>

## Example

### OBS
results/validate/areas_Set1_compare.png


The OBS example uses simulated and invented data and is for illustrative purposes only.  

![Example comparing device versus human tracker at exhibits](set_compare_OBS_cluster.png)

*Visualization of the time discrepancies between the human and device tracker at exhibits.  A light grey indicates when the device location was in the larger cluster of immediately adjacent exhibits (generated when using the [script_validate_clusterLoc.R](script_validate_clusterLoc.md) script).  *