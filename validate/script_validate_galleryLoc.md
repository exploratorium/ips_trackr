# Example - Validating Timing and Tracking Data (Gallery-Level)
[R_scripts/script_gndTruth_gal.R](../R_scripts/script_gndTruth_gal.R)

## Description
The following illustrates how to validate ground-truth data at the gallery-level.  The data used in this example were collected by an experimental IPS at the Exploratorium. These data consist of timestamped longitude and latitude coordinates logged by a mobile device, the IPS tracker, as the person wearing that device, moved through the museum from gallery to gallery.  These data were saved in csv files; there is one such device data file for each whole-museum 'track.' 

Validating the data collected at the gallery-level consists of the following steps:

+	Find the area each long/lat data point is in according to the IPS device and the human tracker. This includes 
    + Running any project-specific data smoothing or cleaning algorithms (e.g., correct ‘impossible’ locations, such as one second jumps across the building and back, paths that go through walls, etc.). The algorithms defined for each project are placed in the file, <*projectName*>/R\_algorithm/algorithm\_<*projectName*>.R .  
	+	Aligning the data collected by the human tracker with the data collected by the devices according to a common clock. 
+	Plot the discrepancy between the human tracker and the device tracker in time and space.
+	Calculate the dwell time discrepancies between the human and device location data.
+	Compute the Cohen's kappa as an interrater reliability metric for the data collected.

The R script shown here serves as a template, to be adapted to process other data collected by other systems. The example data used are **scrubbed and altered** tracking data from the Exploratorium.    They do not capture actual traffic patterns at the museum or the capabilities of the prototype indoor positioning system used to collect these data. 


## Required packages and files

This script uses the following files and R packages.

### User Specified, Input Files

These files specify the galleries, or areas of interest,  their boundaries, and other characteristics:

+ [EXPLO/EXPLO_settings.csv](../EXPLO/EXPLO_settings.csv) - specifies the <*areas_Names_file*> and <*areas_Shape_file*> files for the project. See [<*projectName*>_settings](../settings.md) for a description of the variables.  
+ [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv) - lists the names of the galleries (or other areas of interest) and their colors for any visualization to be generated. See [<*areas_Names_file*>](../areas_Names_file.md) for a description of the columns.
+ [EXPLO/EXPLO_galleryShapes.csv](../EXPLO/EXPLO_galleryShapes.csv) - specifies the long/lat vertices of the galleries listed in [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv). See [<*areas_Shape_file*>](../areas_Shape_file.md) for a description of the columns.  [IPS_TrackJS](https://bitbucket.org/exploratorium/ips_trackjs) can be useful in creating this file from floor plans.

Any project-specific algorithms for cleaning the data (e.g. interpolating missing time values, smoothing the location coordinates to compensate for spurious jumps) are coded in: 

+ [/EXPLO/R_algorithm/algorithm_EXPLO.R](../EXPLO/R_algorithm/algorithm_EXPLO.R) This file is optional.  

The data collected by the IPS devices and the human tracker are:

+ e.g., [EXPLO/dataLogs/Test_02/LocData_V1.csv](../EXPLO/dataLogs/Test_02/LocData_V1.csv) - contains the data collected by the IPS device. See [<*device.datafile*>](../device_datafile.md) for a description of the columns.
+ e.g., [EXPLO/dataLogs/Test_02/humanTrackerLog_V1.csv](../EXPLO/dataLogs/Test_02/humanTrackerLog_V1.csv) - notes when the person being tracked enters a gallery as s/he moved through the museum. See [<*human.datafile*>](human_datafile.md) for a description of its columns.

We list the IPS device data files paired with the data collected by the human tracker in:

+ [EXPLO/dataLogs/fileList_01.csv](../EXPLO/dataLogs/fileList_01.csv) - maps the IPS data files to the data from the human tracker with accompanying information (e.g., the device used to collect the timestamped long/lat data, the subfolder the data file is in, timing information to synchronize the data from the human and the device tracker to a common clock, etc.). See [<*csv_fileList*>](../csv_fileList.md) for a description of its columns.

### R packages
These R packages are required by the script and can be installed from within RStudio.  Running this script will also check for these packages and install any that are missing.

+	sp
+   zoo
+	irr
+	RgoogleMaps
+	rgdal
+	TraMineR


```
# R code to load the needed packages
# check to make sure all the packages needed are installed
required.packages = c("sp","irr","rgdal", "zoo",	"RgoogleMaps", "TraMineR")
new.packages = required.packages[!(required.packages %in% 	
	installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

### R files in this repository

+	[R_scripts/IPS_genFunctions.R](../R_scripts/IPS_genFunctions.R)
+	[R_scripts/validate_latLong.R](../R_scripts/validate_latLong.R)

```
# set the working directory
setwd("..")

# load the necessary functions
source("R_scripts/IPS_genFunctions.R")
source("R_scripts/validate_latLong.R")
```

## Gallery-Level Validation Steps
### A. Find the gallery for each long/lat data point 
The following maps the long/lat coordinates collected by the IPS to mutually exclusive galleries, or areas of interest. 

```
# "EXPLO" contains scrubbed (and altered) location data from the 
# Exploratorium and is for illustrative purposes only.
projectName = "EXPLO"

# The list of data files to align and process are listed in csv_fileList, 
# a csv file you populate and save under projectName/dataLogs
csv_fileList =  "fileList_01.csv"

# create a dataframe from csv_fileList and select only those that have
# a matching human tracker data file
csv_df = read.csv(paste(projectName,"/dataLogs/",csv_fileList, sep=""), 
                  header = TRUE, stringsAsFactors = FALSE)
csv_df = csv_df[!(is.na(csv_df$track_id) ) & csv_df$track_id != "",]
csv_df = csv_df[!(is.na(csv_df$human.datafile) ) & 
	csv_df$human.datafile != "",]

# The following analyses assume the time unit is seconds.  
# If the time data were collected with another time unit,
#  unitsPerSec will be used to scale the time to seconds.
unitsPerSec = 1000  #ms in a second --- the example device logs in ms

# for each IPS data file, find the area each long/lat coordinate was in 
# according to the IPS device and the human tracker
for (i in 1:nrow(csv_df)){
  createFile_areaLocated(projectName,csv_df[i,], unitsPerSec, 
  	file_suffix = "proc")
}
```
This **outputs** a set of _proc.csv files in the same folder as the IPS device data files: 

+ e.g. [EXPLO/dataLogs/Test_02/V6_proc.csv](../EXPLO/dataLogs/Test_02/V6_proc.csv)
See [<*track\_id*>_proc](../track_id_proc.md) for a description of the columns in this file.

### B. Plot the discrepancy between the human tracker and the device tracker

#### B1. According to time
First, we visualize the difference in the galleries visited and each gallery's dwell time, for the IPS device versus the human tracker.  This gives us initial indications of overall agreement and which galleries tend to be confused with each other.

```
# set the time resolution (in seconds) for the analysis  
t_step = 1 

# R code to generate the time discrepancy plots
plot_humanVsDevice(projectName, csv_fileList, csv_df , t_step ,
	file_suffix= "proc",   cluster_p = FALSE)

```
This **outputs** a set of png files. Each png file shows one to three plots.  Each plot  consists of three bars on a common timeline, one representing the human tracker's data, the next representing the corresponding IPS device data, and the last showing where and when they disagree.   The paired human and IPS device data files are specified in csv_fileList =  "fileList_01.csv":  



+ e.g. [EXPLO/results/validate/areas_Set1_compare.png](../EXPLO/results/validate/areas_Set1_compare.png)

![alt text](../EXPLO/results/validate/areas_Set1_compare.png "Gallery location discrepancy according to time")

***Gallery location discrepancy according to time - IPS device vs. human tracker.*** *A plot is generated for each pair of device and human tracking data.   This example plots three of the four device and human pairs and their discrepancy, specified in csv_fileList =  "fileList_01.csv".  The visualization suggests difficulties in the IPS distinguishing the East (green) gallery from the Mezzanine (orange) gallery.*  



+ e.g. [EXPLO/results/validate/contentArea_Set1_compare.png](../EXPLO/results/validate/contentArea_Set1_compare.png)

![alt text](../EXPLO/results/validate/contentArea_Set1_compare.png "Content area discrepancy according to time")

***Content area discrepancy according to time - IPS device vs. human tracker.*** *This plot grouped galleries according to their content, or subject matter, focus.  (Gallery groupings are assigned in [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv)). This example plots three of the four device and human pairs and their discrepancy, specified in csv_fileList =  "fileList_01.csv".*  Compared to the prior set of plots, this visualization shows slightly better agreement for content groupings versus galleries.  


+ e.g. [EXPLO/results/validate/section_Set1_compare.png](../EXPLO/results/validate/section_Set1_compare.png)

![alt text](../EXPLO/results/validate/section_Set1_compare.png "Section location discrepancy according to time")

***Section location discrepancy according to time - IPS device vs. human tracker.*** *In this example, we grouped galleries by position in the museum, front, middle or the back sections of the Exploratorium.   This example plots three of the four device and human pairs and their discrepancy, specified in csv_fileList =  "fileList_01.csv".*  With this segmentation of the museum into larger areas, the agreement is much improved.

#### B2. According to space
We can also plot the discrepancy spatially on a map to determine areas, especially along gallery boundaries, with problematic tracking.

```
# R code to generate the spatial discrepancy plots
map = getGMap(projectName, csv_df, file_suffix= "proc")
for (i in 1:nrow(csv_df)){
  plotErrorMap_GMap(projectName, csv_fileList, csv_df[i,], map, 
  	file_suffix= "proc")
}
```
This **outputs** a set of png files. A png is created for each device tracker data file specified in csv_fileList =  "fileList_01.csv":  


+ e.g. [EXPLO/results/validate/Visitor6_D5_proc_errorMap.png](../EXPLO/results/validate/Visitor6_D5_proc_errorMap.png)

![alt text](../EXPLO/results/validate/Visitor6_D5_proc_errorMap.png "Gallery location discrepancy mapped")

***Gallery location discrepancy - IPS device vs. human tracker.*** *This map plots the long/lat coordinates where the IPS device disagreed with the human tracker in the gallery location.  The  fill color of each dot indicates the gallery location according to the human tracker, while the perimeter color signifies the gallery location according to the IPS device.  Note that distinguishing the East (green) gallery from the Mezzanine (orange) gallery is challenging.  Similarly, locations on the borders of the South (light blue) gallery with the Crossroads (dark blue) and with the Central (medium blue) gallery can bleed into each other.*

### C. Calculate dwell time discrepancies   
Assuming that the human tracker was perfect, we can compute the amount of time the device misidentified a gallery.

```
# R code to calculate the time a device misidentified the area
for (i in 1:nrow(csv_df)){
  createErrorTable(projectName,  csv_df[i,],  file_suffix = "proc" )
}
```

This **outputs** a csv file of dwell time per area for the IPS device versus human tracker:

+ e.g. [EXPLO/results/validate/Visitor6_D5_proc_errorTable.csv](../EXPLO/results/validate/Visitor6_D5_proc_errorTable.csv)
See [<*track_id*>_errorTable](track_id_errorTable.md) for a description of the columns in this file. 

### D. Calculate the Cohen's Kappa

Alternatively,  the following calculates the interrater reliability for each area (individual device vs. human tracker) assuming imperfect human trackers.  

```
# R code to calculate kappa
# this does not count the times when the human tracker lost the person being
# tracked.  Those data are treated as NA.
for (i in 1:nrow(csv_df)){
  computeKappaHumanDevice(projectName, csv_fileList, 
  	csv_df[i,], file_suffix= "proc")
}
```
This **outputs** a text file with the test results:

+ e.g. [EXPLO/results/validate/fileList_01_kappaIndDevice.txt](../EXPLO/results/validate/fileList_01_kappaIndDevice.txt)
See [<*csv_fileList*>\_kappaIndDevice.txt](kappaIndDevice.md) for a description of this file. 


## File Directory Structure
The directory structure of  the files required and generated by this script is shown below.  In this example 

+	<*projectName*> = EXPLO
+	<*areas_Shape_file*> = EXPLO_galleryShapes
+	<*areas_Names_file*> = EXPLO_galleryNames
+	<*device.datafile*> = LocData_V1
+	<*human.datafile*> = humanTrackerLog_V1
+	<*csv_fileList*> = fileList_01
+	<*subfolder*> = Test_02
+	<*track_id*> = V6
+	<*colName*> = areas, contentArea, and section (the column names in <*areas_Names_file*>)

![alt text](directory_gallery.png  "File Directory Structure - Cluster-Level Validation")

***File directory structure*** 
