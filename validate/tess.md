# <*csv_fileList*>_tess.csv

This csv file contains the vertices of the closed polygons that specifies each exhibit area.  An exhibit area is defined as the tile in the Dirichlet  tessellation pattern computed from the centroid location for that exhibit. There is a tessellation tile for each exhibit listed in [<*csv_fileList*>_centroids.csv].  

## Column Description
This is a csv file with four columns. 

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|area| the names of the areas exhibit (or exhibit cluster) of interest | text | 
|longitude | longitude of the area's vertex | float | the first value must be the same as the last value for an area|
|latitude | latitude of the area's vertex | float | the first value must be the same as the last value for an area|


## Example
### OBS
fileList_Xbit_tess.csv


```
"area","longitude","latitude"
"City Viewer",-122.396908,37.8023779999998
"City Viewer",-122.396980759675,37.8023834569754
"City Viewer",-122.396988,37.8023794895911
"City Viewer",-122.396988,37.8023791576899
"City Viewer",-122.396957126137,37.8023448398572
"City Viewer",-122.396908,37.8023779999998
"Pic Place City",-122.39687,37.8023180000002
"Pic Place City",-122.396876,37.802328
"Pic Place City",-122.396917589295,37.8023008926919
"Pic Place City",-122.396892969688,37.8022735267748
"Pic Place City",-122.39687,37.8023180000002
...
"Sighter",-122.396888,37.8023660000002
"Sighter",-122.396892,37.8023829999998
"Sighter",-122.396908,37.8023779999998
"Sighter",-122.396957126137,37.8023448398572
"Sighter",-122.396933759984,37.8023188672164
"Sighter",-122.396888,37.8023660000002
...
"Sed Core",-122.396723,37.8024369999999
"Sed Core",-122.396768107943,37.8024999809015
"Sed Core",-122.396832626693,37.8024646274249
"Sed Core",-122.396828,37.8024579999999
"Sed Core",-122.396742,37.8024190000001
"Sed Core",-122.396723,37.8024369999999
```

[script_validate_xbitLoc.R]: <script_validate_xbitLoc.md>
[<*csv_fileList*>_centroids.csv]: <../centroids.md>
