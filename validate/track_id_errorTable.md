# <*track_id*>_errorTable.csv

This csv file contains a table of dwell time per area (as specified in [<*areas_Names_file*>](areas_Names_file.md) according to the human versus the device tracker. 


## Column Description 


| column name | description | format |notes |
|----------|---------|-------------|-------------|
|areas|  area of interest; an area defined in <*projectName*>/[<*areas_Shape_file*>]| text | 
|true.dwell.time |  total dwell time for that area according to the human tracker   | hh:mm:ss |  |
|device.dwell.time |  total dwell time for that area according to the device tracker | hh:mm:ss |   |
|truePos | time that human and device trackers agreed the person/object being tracked was in that area |hh:mm:ss  |  |
|falsePos | time that device tracker reported the person/object being tracked was in that area but the human tracker reported being in another area | hh:mm:ss|  |
|falseNeg  | time that device tracker reported the person/object being tracked was NOT in that area but the human tracker reported being in that area | hh:mm:ss| 	|

[<*areas_Shape_file*>]: <areas_Shape_file.md>

## Example
### EXPLO
results/validate/Test01_D1_proc_errorTable.csv

The EXPLO example uses scrubbed data and is for illustrative purposes only.  

```
"areas","true.dwell.time","device.dwell.time","truePos","falsePos","falseNeg"
"Entry","00:05:41","00:06:33","00:04:50","00:01:44","00:00:52"
"West","00:02:54","00:04:58","00:02:48","00:02:10","00:00:06"
"Forum","00:01:12","00:00:00","00:00:00","00:00:00","00:01:12"
"Xroads","00:06:28","00:04:51","00:04:37","00:00:14","00:01:51"
...
"Ramp","00:00:21","00:01:57","00:00:05","00:01:53","00:00:17"
"Mid Stairs","00:00:15","00:00:00","00:00:00","00:00:00","00:00:15"
```

### OBS
results/validate/Test3_S1_proc_errorTable.csv



The OBS example uses simulated and invented data and is for illustrative purposes only.  

```
"areas","true.dwell.time","device.dwell.time","truePos","falsePos","falseNeg"
"City Viewer","00:00:59","00:01:06","00:00:10","00:00:56","00:00:49"
"Pic Place City","00:01:51","00:01:11","00:00:21","00:00:51","00:01:30"
"Shift Shore","00:01:47","00:01:36","00:00:19","00:01:18","00:01:28"
"Sighter","00:02:51","00:01:48","00:01:04","00:00:44","00:01:47"
...
"Tidal Ribbon","00:02:25","00:01:55","00:00:43","00:01:12","00:01:42"
"Mud Slide","00:01:27","00:01:06","00:00:21","00:00:46","00:01:06"
"Sed Core","00:03:09","00:02:35","00:01:24","00:01:12","00:01:46"
```