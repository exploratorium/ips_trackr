# <*csv_fileList*>_centroids.csv

This csv file contains the information about the centroids computed for each exhibit (or area of interest) and the location data  used to compute each centroid. Only those exhibits for which the human tracker collected enough data (at the exhibit for at least 30 seconds) are included.


## Column Description 
This is a csv file with at at least 7 columns. 

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|xbit  |  exhibit name| text | a subset of exhibits listed in <*projectName*>/[<*areas_Names_file*>] |
|centroid_x |  longitude of the centroid| float |  |
|centroid_y | latitude of the centroid| float |  |
|r_sd | standard deviation of the error[1]  | float | in meters|
|r_mean| mean  error[1] | float| in meters
|r_max |max error[1] | float| in meters
|adjacentXbits | exhibits adjacent to the exhibit  | text|  users should edit this entry to fit with the actual adjacencies on the floor (e.g.,mathematically adjacent exhibit may be separated by physical walls) |

[1] Error is the distance between the lat/long logged by the device while the human tracker was at the exhibit, and the centroid

[<*csv_fileList*>.csv]: </csv_fileList.md>
[<*areas\_Shape_file*>]: <areas_Shape_file.md>

## Example
### OBS
fileList_Xbit_centroids.csv

```
"xbit","centroid_x","centroid_y","r_sd","r_mean","r_max","adjacentXbits"
"City Viewer",-122.396948997513,37.8023702509561,1.48439460225062,3.8056246031778,7.9128958468285,"Sighter-Tide Col"
"Pic Place City",-122.396894429477,37.80229890298,1.70272768872633,2.95704568063119,7.21284275275427,"Shift Shore-Bricks-Library"
"Shift Shore",-122.396909716285,37.8023226312422,1.67078605437362,3.59947668830076,7.18732673420643,"Pic Place City-Sighter-Library-Map Table"
"Sighter",-122.39693069973,37.8023429700312,1.59812366062893,2.80259257594136,7.92693818801078,"City Viewer-Shift Shore-Tide Col-Map Table"
...
"Tidal Ribbon",-122.396691227533,37.8024352782837,1.61462253019376,2.77387533026176,8.43457151836585,"Bay Model-Pic Place Bay-Mud Slide"
"Mud Slide",-122.396713436156,37.8024599776292,1.82315576944788,3.77660945378363,8.36442963390104,"Bay Model-Tidal Ribbon-Sed Core"
"Sed Core",-122.396748814291,37.802434853353,1.85163567046225,3.024782011511,8.74664856577447,"VizWall Bench-Bay Model-Small Model-Mud Slide"
```

[script_validate_xbitLoc.R]: <script_validate_xbitLoc.md>
[<*areas_Names_file*>]: <../areas_Names_file.md>