# Example - Validating Timing and Tracking Data (Cluster-Level)
[R_scripts/script_gndTruth_cluster.R](../R_scripts/script_gndTruth_cluster.R)

## Description
This example illustrates the analysis of ground-truth test data collected according to the [protocol](protocol_validate_clusterLoc.md) defined for exhibit clusters.  It is similar to the process followed to assess timing and tracking data at the exhibit level, and involves the following steps:  

+ Define the area of each exhibit cluster as a set of tiles in a tessellation pattern.  This includes:
     - Finding the location data for each exhibit stop.
     - Calculating the center of mass (centroid) for the location data for each exhibit.
     - Computing the tessellation pattern.  The exhibit cluster consists of the tiles for the exhibit and its neighbors.
+ Plot the location points that fell inside and outside each exhibit and its neighbors' tiles.
+ Find the exhibit area each long/lat data point is in, according to the IPS device and the human tracker. This includes aligning the data collected by the human tracker with the data collected by the devices according to a common clock. 
+ 	Plot the discrepancy between the human tracker and the device tracker.
+	Calculate the dwell time discrepancies between the human and device exhibit cluster  data.


This example uses **simulated** tracking data for the Observatory gallery at the Exploratorium. It is the same dataset used to demonstrate the validation of exhibit-level timing and tracking data. This example serves illustrative purposes only and does not reflect the true  capabilities of the prototype indoor positioning system used in actual tests at the Exploratorium.

## Required packages and files

This script uses the following files and R packages.

### User Specified, Input Files
These files specify the gallery, its boundaries, its exhibits, and other characteristics:

+ [OBS/OBS_settings.csv](../OBS/OBS_settings.csv) - specifies the <*areas_Names_file*> and <*areas_Shape_file*> files for the project. See [<*projectName*>_settings](../settings.md) for a description of the variables.  
+ [OBS/OBS_xbitNames.csv](../OBS/OBS_xbitNames.csv) - lists the names of the exhibits inside the gallery and their colors for any visualization to be generated. See [<*areas_Names_file*>](../areas_Names_file.md) for a description of the columns.
+ [OBS/OBS_galleryShape.csv](../OBS/OBS_galleryShape.csv) - specifies the long/lat vertices of the overall gallery. See [<*areas_Shape_file*>](../areas_Shape_file.md) for a description of the columns.  [IPS_TrackJS](https://bitbucket.org/exploratorium/ips_trackjs) can be useful in creating this file from floor plans.

The data collected by the IPS devices and the human tracker are:

+ e.g., [OBS/dataLogs/Test_03/LocData_S1.csv](../OBS/dataLogs/Test_03/LocData_S1.csv) - contains the data collected by the IPS device. See [<*device.datafile*>](../device_datafile.md) for a description of the columns.
+ e.g., [OBS/dataLogs/Test_03/humanTrackerLog.csv](../OBS/dataLogs/Test_03/humanTrackerLog.csv) - notes when the person being tracked stops at an exhibit in the Observatory gallery. See [<*human.datafile*>](human_datafile.md) for a description of its columns.


We list the IPS device data files paired with the data collected by the human tracker in:

+ [OBS/dataLogs/fileList_Xbit.csv](../OBS/dataLogs/fileList_Xbit.csv) - maps the IPS data files to the data from the human tracker with accompanying information (e.g., the device used to collect the timestamped long/lat data, the subfolder the data file is in, timing information to synchronize the data from the human and the device tracker to a common clock, etc.). See [<*csv_fileList*>](../csv_fileList.md) for a description of its columns.



### R packages
These R packages are required by the script and can be installed from within RStudio.  Running this script will also check for these packages and install any that are missing.

+	spatstat
+	sp
+	irr
+	rgdal
+	RgoogleMaps
+	TraMineR

```
# R code to load the needed packages
# check to make sure all the packages needed are installed
required.packages <- c("spatstat", "sp","irr","rgdal", "RgoogleMaps",
	"TraMineR")
new.packages <- required.packages[!(required.packages %in% 
	installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

### R files in this repository

+	[R_scripts/IPS_genFunctions.R](../R_scripts/IPS_genFunctions.R)
+	[R_scripts/create_tessFunctions.R](../R_scripts/create_tessFunctions.R)
+	[R_scripts/validate_latLong.R](../R_scripts/validate_latLong.R)



```
# set the working directory
setwd("..")

# load the necessary functions
source("R_scripts/IPS_genFunctions.R")
source("R_scripts/create_tessFunctions.R")
source("R_scripts/validate_latLong.R")
```


## Cluster-Level Validation Steps
### A. Define the area of each exhibit as a tile in a tessellation pattern 
The following uses the ground-truth test data to identify the centroid for each exhibit stop and computes the tessellation pattern for the exhibits in the gallery. 

First, set the required variables:
```
# "OBS" contains simulated location data from the 
# Exploratorium's Observatory gallery 
projectName = "OBS"

# The list of data files to align and process are listed in csv_fileList, 
# a csv file you populate and save under projectName/dataLogs
csv_fileList =  "fileList_Xbit.csv"

# create a dataframe from csv_fileList and select only those that have
# a matching human tracker data file
csv_df = read.csv(paste(projectName,"/dataLogs/",csv_fileList, sep=""), 
                  header = TRUE, stringsAsFactors = FALSE)
csv_df = csv_df[!(is.na(csv_df$track_id) ) & csv_df$track_id != "",]
csv_df = csv_df[!(is.na(csv_df$human.datafile) ) & 
	csv_df$human.datafile != "",]

# The following analyses assume the time unit is seconds.  
# If the time data were collected with another time unit,
#  unitsPerSec will be used to scale the time to seconds.
unitsPerSec = 1000  #ms in a second --- the example device logs in ms

# this is the time resolution (in sec) for the analyses.  
t_step = 1 
```

Align each device data file with human tracker data and thereby identify the location data for each exhibit stop. 

```
# align IPS device and human data files
for (i in 1:nrow(csv_df)){
  
  # read in the data and create the dataframe
  fileFolder = paste(projectName,"/dataLogs/",csv_df$subfolder[i], sep="")
  data = prepData(projectName, csv_df[i,], unitsPerSec )
  
  # if there is human tracking data
  if(csv_df$human.datafile[i] != "" & !is.na(csv_df$human.datafile[i]) ){
    
    # clean human tracking data (removing empty lines in the csv file)
    csv_df$human.datafile[i] =  paste(gsub(".csv", "",csv_df$human.datafile[ i]), 
                                       ".csv", sep = "")
    rmEmptyRows(paste(projectName,"/dataLogs/", csv_df$subfolder[i],
                      "/",csv_df$human.datafile[i], sep=""))
    
    # align with the device data
    data = alignHumanDevice(projectName,csv_df[i,], data)
  }
  
  fname = paste(projectName,"/dataLogs/",csv_df$subfolder[i],
  	"/",csv_df$track_id[i] ,"_proc.csv",sep="")        
  write.table(data, fname, sep=",", row.names=FALSE)
}
```
This outputs a set of _proc.csv files in the same folder as the IPS device data files that is next used  to identify the centroid for each exhibit.

```
# find centroid for each xbit and generate centroids.csv file. 
centroids.df = create_centroids (projectName , csv_fileList, 
	csv_df, file_suffix = "proc")
```
This **outputs** a csv file with the long/lat centroid coordinates for each exhibit: 

+ [OBS/fileList_Xbit_centroids.csv](../OBS/fileList_Xbit_centroids.csv)
See [<*csv_fileList*>_centroids.csv](centroids.md) for a description of the columns in this file.

The centroids can now be used to compute the Dirichlet tessellation; each tile of the resulting tessellation defines  each exhibit area.
```
# create tesselation bounds and write tess boundary file 
# adds a column to the centroids file listing the adjacent xbits 
# (i.e., neighbors in the tessellations pattern)
tess.df =create_tessBounds(projectName, csv_fileList,centroids.df )
```
This **outputs** a csv file with the long/lat coordinates for the vertices that define each exhibit's tessellation tile: 

+ [OBS/fileList_Xbit_tess.csv](../OBS/fileList_Xbit_tess.csv)
See [<*csv_fileList*>_tess.csv](tess.md) for a description of the columns in this file.

Calling create_tessBounds also creates an additional column in [OBS/fileList_Xbit_centroids.csv](../OBS/fileList_Xbit_centroids.csv), listing each exhibit's mathematically adjacent exhibits (i.e., shares an edge in the tessellation pattern).  Depending on the floor plan, a mathematical adjacency does not necessarily translate to physical adjacency.  For example, there may be a wall between two supposed neighboring exhibits.  We recommend editing this last column to reflect actual adjacencies.

### B. Plot the location points that fall within and without an exhibit cluster
We visualize where the long/lat coordinates fall for an exhibit stop.  Good cluster-level resolution for that exhibit means that  the majority of the IPS device data points collected when the human tester was at the exhibit fall within the exhibit's and its neighbors' tessellation tiles.  The following looks at the exhibit cluster-level resolution for the Bay Model exhibit in the Observatory gallery.


```
# draw plots of where the points land in tesselation pattern
# use the following to control the plots
plotPts_xbit= "Bay Model"  # which xbit would you like to plot?  
# "all" generates a map plot for each xbit in the tessellation
plotAllTess_p = TRUE  # plot tessellation pattern in background?
plotColor_p = TRUE  # color code tessellation pattern? 
plotPts_p = TRUE  # plot location data points?
plotCentroid_p= FALSE # plot centroids? 
plotLabel_p = FALSE # add xbit name labels? 
plotCluster_p= TRUE # plot cluster xbits? 
plotInd_p= TRUE # highlight the individual xbit?

plot_tessLat(projectName , csv_fileList, csv_df, file_suffix = "proc", 
	plotPts_xbit , plotAllTess_p , plotColor_p ,
    plotCentroid_p, plotLabel_p , plotInd_p , 
    plotPts_p,plotCluster_p )
```
This **outputs** a  png file that plots the long/lat coordinates of each data point logged by the IPS device when the human tester was stopped at the Bay Model exhibit: 

+ [OBS/results/validate/pic/fileList_Xbit_Bay_Model.png](../OBS/results/validate/pic/fileList_Xbit_Bay_Model.png)

![alt text](../OBS/results/validate/pic/fileList_Xbit_Bay_Model.png "Location data logged when human tester was stopped at Bay Model")

***Location data logged when human tester was stopped at Bay Model.*** *A dot indicates a data point collected by the device trackers while the human tracker was at the exhibit. The shaded tiles are the Bay Model's adjacent exhibits.*

### C. Find the exhibit area each long/lat data point is in 
The subsequent analyses are similar to those used to assess timing and tracking data at the gallery  and the exhibit level.
```
source("R_scripts/validate_latLong.R")

# read in the tessellation file 
tess.df = read.csv(paste(projectName,"/",gsub(".csv", "", csv_fileList),
		"_tess.csv", sep=""), 
    header=T, stringsAsFactors = FALSE)
```
Map the long/lat coordinates collected by the IPS to exhibit stops as noted by the human tracker. 
```
# for each device data file, find the area and align device with human tracker data
for (i in 1:nrow(csv_df)){
  
  # read in the data and create the dataframe
  fileFolder = paste(projectName,"/dataLogs/",csv_df$subfolder[i], sep="")
  data = prepData(projectName, csv_df[i,], unitsPerSec )
  
  # find the area according to lat/long coordinates 
  data = findArea(projectName, data, tess.df)
  
  # clean the data according to algorithms defined for that project
  # defined in /R_algorithm/algorithm_<projectName>.R
  if(length(list.files(path = paste(projectName, "/R_algorithm", sep=""), 
                       pattern = paste("algorithm_",projectName,".R", sep = "")) )== 1){
    source(paste(projectName, "R_algorithm", 
                 list.files(path = paste(projectName, "/R_algorithm", sep=""),
                            pattern = paste("algorithm_",projectName,".R", sep = "")), 
                 sep = "/"))
    cat(csv_df$track_id[i], '\n')
    data = algorithm(projectName,data)
  }
  
  # if there is human tracking data
  if(csv_df$human.datafile[i] != "" & !is.na(csv_df$human.datafile[i]) ){
    
    # clean human tracking data (removing empty lines in the csv file)
    csv_df$human.datafile[i] =  paste(gsub(".csv", "",csv_df$human.datafile[ i]), 
                                      ".csv", sep = "")
    rmEmptyRows(paste(projectName,"/dataLogs/", csv_df$subfolder[i],
                      "/",csv_df$human.datafile[i], sep=""))
    
    # align with the device data
    data = alignHumanDevice(projectName,csv_df[i,], data)
  }
  
  fname = paste(projectName,"/dataLogs/",csv_df$subfolder[i],
  	"/",csv_df$track_id[i] ,"_proc.csv",sep="")        
  write.table(data, fname, sep=",", row.names=FALSE)
}
```
This **outputs** a set of _proc.csv files in the same folder as the IPS device data files: 

+ e.g. [OBS/dataLogs/Test_03/Test3_S1_proc.csv](../OBS/dataLogs/Test_03/Test3_S1_proc.csv)
See [<*track\_id*>_proc](../track_id_proc.md) for a description of the columns in this file.

### D. Plot the discrepancy between the human tracker and the device tracker 
#### D1. According to time
We plot the times when the IPS device was able to locate the exhibit cluster for exhibit stops noted by the human tracker.

```
# R code to generate the time discrepancy plots
plot_humanVsDevice(projectName, csv_fileList, csv_df , t_step ,
	file_suffix= "proc",   cluster_p = FALSE)
```

This **outputs** a set of png files. Each png file shows one to three plots.  Each plot  consists of three bars on a common timeline, one representing the human tracker's data, the next representing the corresponding IPS device data, and the last showing where and when (1) they agree on the exhibit stop, (2) the IPS identified a neighboring exhibit, and (3) the IPS identified an exhibit stop that was neither the exhibit nor one of its adjacent neighbors according to the human tracker.   The paired human and IPS device data files are specified in csv_fileList =  "fileList_Xbit.csv": 

+ e.g. [OBS/results/validate/areas_ClusterSet1_compare.png](../OBS/results/validate/areas_ClusterSet1_compare.png)

![alt text](../OBS/results/validate/areas_ClusterSet1_compare.png "Gallery location discrepancy according to time")

***Exhibit cluster  discrepancy according to time - IPS device vs. human tracker.*** *A plot is generated for each pair of device and human tracking data.   There are some times when the IPS identified an exhibit cluster (in grey on the top bar) or the exhibit itself (in black on the top bar), but there is  a great deal of red indicating disagreement between the IPS and human tracker in the exhibit cluster identified.*


#### D2. According to space
To look at the spatial discrepancy, we map the long/lat data points for which the human tracker indicated one exhibit  but the IPS device located another exhibit that was not a neighboring exhibit.  

```
# R code to generate the spatial discrepancy plots
map = getGMap(projectName,  csv_df, file_suffix= "proc")
for (i in 1:nrow(csv_df)){
  plotErrorMap_GMap(projectName, csv_fileList, csv_df[i,], map, 
  	file_suffix= "proc", areaShapes = tess.df, cluster_p = TRUE)
}
```
This **outputs** a set of png files. A png is created for each device tracker data file specified in csv_fileList =  "fileList_Xbit.csv": 

+ e.g. [OBS/results/validate/Test3_S1_proc_Cluster_errorMap.png](../OBS/results/validate/Test3_S1_proc_Cluster_errorMap.png)

![alt text](../OBS/results/validate/Test3_S1_proc_Cluster_errorMap.png "Gallery location discrepancy mapped")

***Exhibit cluster location discrepancy - IPS device vs. human tracker.*** *A dot indicates a data point for which the human and the device trackers disagreed (i.e., the human tracker identified an exhibit and the IPS device location corresponds to a different exhibit that is not an exhibit  adjacent to the exhibit the human tracker was at). The position of the point is the longitude and latitude coordinates as logged by the device. The fill of a point is the color of the exhibit according to the human tracker. The perimeter color indicates the exhibit according to the device tracker.  This map indicates that the IPS does not support cluster-level tracking well.*

### E. Calculate the cluster dwell time discrepancies (human vs. device) 

Assuming that the human tracker was perfect, this calculates the time the IPS device misidentified the cluster location.

```
# R code to calculate the time a device misidentified the cluster
for (i in 1:nrow(csv_df)){
  createErrorTableCluster(projectName, csv_fileList,  csv_df[i,],  
  	file_suffix = "proc" )
}
```

This **outputs** a csv file for each entry in csv_fileList =  "fileList_Xbit.csv" of the amount of time a device misidentified the exhibit cluster for an exhibit stop:

+ e.g. [OBS/results/validate/Test3_S1_proc_clusterErrTable.csv](../OBS/results/validate/Test3_S1_proc_clusterErrTable.csv)
See [<*track_id*>_clusterErrTable](clusterErrTable.md) for a description of the columns in this file. 


## File Directory Structure
The directory structure  of the files required and generated by this script is shown below.  In this example 

+	<*projectName*> = OBS
+	<*areas_Shape_file*> = OBS_galleryShape
+	<*areas_Names_file*> = OBS_xbitNames
+	<*device.datafile*> = sample9920
+	<*human.datafile*> = humanTracker
+	<*csv_fileList*> = fileList_Xbit
+	<*subfolder*> = Test_03
+	<*track_id*> = Test3_S1
+	<*xbit*> = Bay Model
+	<*colName*> = areas (the column name in <*areas_Names_file*>)

![alt text](directory_cluster.png  "File Directory Structure - Cluster-Level Validation")

***File directory structure*** 



