## Exhibit-level Validation Protocol
The ground truth protocol for exhibit-level testing is similar to that for gallery-level validation except that an exhibit often does not have clearly defined physical boundaries, especially in an open floor plan.  The following procedure identifies exhibit boundaries by calculating the centroid of the position data collected while a human tester was using each exhibit from that exhibit’s multiple use points (i.e., any spot from which a visitor may interact with that exhibit), and computing the Dirichlet tessellation pattern based on those centroids.  Each tessellation tile then defines the area and hence the boundaries of each exhibit. Note that this protocol defines an exhibit area that includes the space between exhibits and is, therefore, larger than the actual area of use.  Nonetheless, it can provide an initial indication of whether or not exhibit-level timing and tracking data can be acquired by the IPS.

### A. Collect ground truth data

1.	Synchronize the mobile devices’ time with the human tester’s watch.
2.	Turn on the devices’ tracking application.
3.	Human tester wears all the devices around his/her waist.
4.	Human tester stops at each exhibit.
    1.	Stands/sits still for at least 30 seconds in each spot from which the exhibit can be used.
    2.	Notes the time when she is at each position at each exhibit.
5.	Download the data from the mobile devices.


![alt text](xbitProtocol_F1.png "Collecting Ground Truth Data")

***Human tracker stands (or sits) at each use position for each exhibit for at least 30 seconds.***

_ _ _

### B. Process and assess the data 
The [accompanying script](script_validate_xbitLoc.md) supports the analysis of the ground-truth data collected.  More specifically, it:

1.	Defines the boundaries of each exhibit. 
    1.	Finds the location data for each exhibit stop.
    2.	Calculates the center of mass (centroid) for the location data for each exhibit.
    3.	Computes the tessellation pattern.
2.	Aligns device location data with the data collected by the  human tracker.
3.	Overlays the location data on the tessellation pattern.
4.	Determines location discrepancies (point  locations that fall within and outside exhibit boundaries).
5.	Calculates the dwell time discrepancies between the human and device location data.
6.	Computes the Cohen's kappa as an interrater reliability metric for the data collected.



![alt text](xbitBoundaries_F2.png "Processing Ground Truth Data")

***Compute the tessellation pattern using the centroid of each exhibit.*** *Each tile defines each exhibit’s area and boundaries.*


_ _ _

![alt text](xbitProtocol_F3.png "Processing Ground Truth Data")

***Align the data collected by the human tracker with the device tracker.*** 
