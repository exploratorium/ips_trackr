# <*csv_fileList*>_centroidErrTable.csv

This csv file  contains the calculated number of false and true positive and false and true negative location points for each exhibit.


## Column Description 


| column name | description | format |notes |
|----------|---------|-------------|-------------|
|xbit  |  exhibit name| text | exhibits listed in [<*csv_fileList*>_centroids.csv] |
|truePos |  number of location points that fell within the exhibit boundaries when the human tracker was at the exhibit| int |  |
|falsePos | number of location points that fell within the exhibit boundaries when the human tracker was **NOT** at the exhibit| int |  |
|falseNeg | number of location points that fell outside the exhibit boundaries when the human tracker was at the exhibit| int | |
|trueNeg| number of location points that fell outside the exhibit boundaries when the human tracker was **NOT** at the exhibit | int| |
|falsePosXbits | exhbit where the false positives fell  | text| exhibit names


[<*areas_Shape_file*>]: <../areas_Shape_file.md>

## Example
###OBS
results/validate/fileList_Xbit_centroidErrTable.csv

```
"xbit","truePos","falsePos","falseNeg","trueNeg","falsePosXbits"
"City Viewer","8","69","52","3445","-Shift Shore-Sighter-Tide Col-"
"Pic Place City","31","46","87","3410","-Shift Shore-Sighter-Sky Theatre-Bricks-Library-Map Table-"
"Shift Shore","26","71","94","3383","-City Viewer-Pic Place City-Sighter-Bricks-Library-Map Table-"
"Sighter","66","53","113","3342","-City Viewer-Pic Place City-Shift Shore-Tide Col-Map Table-"
...
"Tidal Ribbon","58","78","122","3316","-Draw Bay-Bay Model-Pic Place Bay-Mud Slide-Sed Core-"
"Mud Slide","26","58","94","3396","-Bay Model-Pic Place Bay-Tidal Ribbon-Sed Core-"
"Sed Core","109","94","131","3240","-Draw Bay-VizWall Bench-Bay Model-Small Model-Vitrine-Wired Pier-Tidal Ribbon-Mud Slide-"
```


[<*csv_fileList*>_centroids.csv]: <centroids.md>