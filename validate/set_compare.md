# <*colName*>_Set\_compare.png

This png file visualizes the time alignment of the sequence of locations the human versus the device tracker identified, and shows the discrepancies. A visualization is produced for each device.datafile specified in <*projectName*>/dataLogs/[<*csv_fileList*>.csv]  

[<*csv_fileList*>.csv]: </csv_fileList.md>

## Example
### EXPLO
results/validate/areas_Set1_compare.png

The EXPLO example uses scrubbed data and is for illustrative purposes only.  

![Example comparing device versus human tracker at galleries](set_compare_EXPLO.png)

*Visualization of the time discrepancies between the human and device tracker in galleries*

### OBS
results/validate/areas_Set1_compare.png


The OBS example uses simulated and invented data and is for illustrative purposes only.  

![Example comparing device versus human tracker at exhibits](set_compare_OBS.png)

*Visualization of the time discrepancies between the human and device tracker at exhibits*