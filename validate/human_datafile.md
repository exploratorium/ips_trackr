# <*human.datafile*>.csv

This csv file is the timing and tracking data collected by the human tracker. 
## Column Description 
This is a csv file with at least 2 columns. A user can have additional columns, but they are  not processed by the R scripts. Rows are chronologically ordered.

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|Enters.into| area of interest the person carrying tracking device has entered into| float | 'gallery' is used to note when the person/object being tracked is in interstitial space or when the human tracker has lost the track; always used on the last row to note when the tracking has stopped.
|Watch.time |  time on the human tracker's stop watch when the person carrying the tracking device enters into an area of interest | hh:mm:ss | 
|<*otherCol*> | additional column for notes |  | optional


## Example
### EXPLO
dataLogs/Test_01/humanTrackerLog.csv

```
"Enters.into","Watch.time","Notes"
"Entry","0:00:00","start tracking"
"Xroads","0:11:55",""
"West","0:15:09","15:57 into Cinema; 16:40 into West; 17:00 at Glasses; 17:15 off glasses"
...
"Outdoor","0:47:00",""
"Entry","0:48:18",""
"gallery","0:49:12","ends tracking"
```

### OBS
dataLogs/Test_03/humanTrackerLog.csv

```
Enters.into,Watch.time,Notes
gallery,14:02:23,
Tide Col,14:19:25,spot 1
gallery,14:20:38,
Tide Col,14:20:39,spot 2
gallery,14:21:27,
City Viewer,14:21:35,spot 1
gallery,14:22:35,
Sighter,14:23:18,spot 1
gallery,14:24:10,
Sighter,14:24:15,spot 2
gallery,14:25:18,
Sighter,14:25:22,spot 3
gallery,14:26:17,
...
Library,15:17:12,spot 4
gallery,15:18:04,
Map Table,15:18:26,spot 1
gallery,15:19:07,
Map Table,15:19:12,spot 2
gallery,15:19:59,
Map Table,15:20:03,spot 3
gallery,15:20:59,
Map Table,15:21:05,spot 4
gallery,15:22:03,
Map Table,15:22:08,spot 5
gallery,15:23:01,
Map Table,15:23:06,spot 6
gallery,15:24:07,
Map Table,15:24:13,spot 7
gallery,15:25:03,
Map Table,15:25:08,spot 8
gallery,15:26:04,
```
