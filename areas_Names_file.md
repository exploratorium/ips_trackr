# <*areas_Names_file*>.csv

This csv file is used to specify the area names and each area's color to be used in the graphics the R scripts generate. 

## Column Description 
This is a csv file with at least two columns (areas and color).  It can contain additional column pairs, with the second column specifying the color for the first column within each pair.  The color column must have the word 'color' in its name.



| column name | description | format |notes |
|----------|---------|-------------|-------------|
|areas| the names of the areas (e.g. galleries, clusters, exhibits) of interest; 'gallery' is used to designate when the location is unknown | text | mandatory column; do not change the column name
|color | the color for each area; used in generating the visualizations for analysis | RGB in HEX | mandatory column; do not change the column name
|<*groupX*> | area's group membership.  This can only take one value.  | text | optional column; the column name is user defined
|<*groupX*>_color | the color for the <*groupX*> membership | RGB in HEX | mandatory if there is a <*groupX*> column; the column name must contain the word 'color'


## Example

###  EXPLO
EXPLO_xbitNames.csv

```
areas,color,contentArea,contentArea_color,section,section_color
gallery,#010101,unknown,#010101,unk,#010101
Entry,#999999,transit,#aaaaaa,front,#984ea3
West,#984ea3,social,#984ea3,front,#984ea3
Forum,#FF81D9,event,#FF81D9,front,#984ea3
Xroads,#104BA9,physics,#1f78b4,mid,#80b1d3
...
Mezz,#FF8500,environment,#BF9930,up,#FFBE00
East,#4daf4a,biology,#4daf4a,back,#4daf4a
...
Ramp,#BF9930,environment,#BF9930,up,#FFBE00
Mid Stairs,#888888,transit,#aaaaaa,back,#4daf4a
```

###  OBS
OBS_xbitNames.csv

```
areas,color,group1,group1_color
gallery,#010101,gallery,#010101
City Viewer,#5C0000,citySide,#E59C33
Pic Place City,#DAB35E,citySide,#E59C33
Shift Shore,#FF8D31,citySide,#E59C33
Sighter,#B88A00,citySide,#E59C33
...
Tidal Ribbon,#3288bd,baySide,#5076CC
Mud Slide,#7B69D5,baySide,#5076CC
Sed Core,#542788,baySide,#5076CC
```