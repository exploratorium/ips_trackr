# <*track\_id*>_proc.csv

This csv file contains processed data with locations (e.g., galleries, exhibits, or other areas of interest) information. 


## Column Description 
This is a csv file with at least 5 columns. There can be additional columns that have been copied over from the corresponding device.datafile file specified in <*projectName*>/dataLogs/[<*csv_fileList*>.csv]. 

| column name | description | format |notes |
|----------|---------|-------------|-------------|
|latitude|  latitude| float | copied  from  corresponding device.datafile |
|longitude |  longitude | float | copied  from  corresponding device.datafile |
|time | time from when device started tracking | float | time units are user-specified in the unitsPerSec  variable in the R script |
|<*otherCol*> | additional data collected by device |  | copied  from  corresponding  device.datafile|
|area_true| area that human tracker identified | text| possible areas are given in <*projectName*>/[<*areas\_Shape_file*>]; 'gallery' is used when the person/object tracked is in interstitial space, when the tracking has either not begun or has ended, or when the human tracker has lost the track.
|area_located | area that device tracker identified | text| possible areas are given in <*projectName*>/[<*areas\_Shape_file*>]; 'gallery' is used if the location coordinates fall outside of the possible tracking area.

[<*csv_fileList*>.csv]: </csv_fileList.md>
[<*areas\_Shape_file*>]: <areas_Shape_file.md>

## Example
### EXPLO
Test_01/Test01_D1_proc.csv

```
"latitude","longitude","time","floorNum","area_true","area_located"
37.8012869019716,-122.39838438974,433.243,1,"Entry","Entry"
37.8012869019716,-122.39838438974,433.969,1,"Entry","Entry"
37.8012869019716,-122.39838438974,434.928,1,"Entry","Entry"
37.801287473895,-122.39838438974,436.535,1,"Entry","Entry"
...
37.8018161005923,-122.396876666592,2418.461,1,"East","Central"
37.8018162572434,-122.396884372228,2419.576,1,"East","Central"
37.8018220319998,-122.396893571967,2420.408,1,"East","Central"
37.8018244838454,-122.396906520226,2422.185,1,"Central","Central"
37.8018256715373,-122.396906520226,2423.188,1,"Central","Central"
37.8018278118734,-122.396906520226,2424.188,1,"Central","Central"
....
37.8012582534458,-122.398444766125,3035.471,1,"gallery","Entry"
37.80125741508,-122.398445416571,3036.461,1,"gallery","Entry"
37.80125741508,-122.398445416571,3037.459,1,"gallery","Entry"
37.80125741508,-122.398445416571,3038.437,1,"gallery","Entry"
```

### OBS
Test_03/Test3_S1_proc.csv

```
"latitude","longitude","time","floorNum","area_true","area_located"
37.8022425,-122.3968651,189.085,2,"gallery","gallery"
37.8023378128995,-122.396881214879,190.722,2,"gallery","Shift Shore"
37.8023385029025,-122.39687949244,192.865,2,"gallery","Shift Shore"
37.8023419492032,-122.396891724921,192.865,2,"gallery","Shift Shore"
...
37.8023253077486,-122.396936561914,1296.219,2,"Sighter","Sighter"
37.8023143606621,-122.396923120673,1298.137,2,"Sighter","Shift Shore"
37.8023731735799,-122.396917757595,1298.49,2,"Sighter","City Viewer"
....
37.802353051701,-122.396884918075,5271.394,2,"gallery","Shift Shore"
37.8023570097983,-122.39690264061,5271.41,2,"gallery","Sighter"
37.8023571149161,-122.396899929225,5273.396,2,"gallery","Sighter"
```