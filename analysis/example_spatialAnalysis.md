# Spatial Analysis - Example
[R_scripts/script_analysis_spatial.R](../R_scripts/script_analysis_spatial.R)

## Description
This example uses scrubbed data collected by a Wi-Fi based indoor positioning system that logged the timestamped location (longitude and latitude coordinates) of a mobile tracking device worn by visitors as they moved through the Exploratorium.  The analysis identifies the gallery, in which the long/lat coordinates fall, and generates static plots of location information on a map. For interactive exploratory analysis of the IPS data files, consider also using the web-based tool, [IPS_TrackJS](https://bitbucket.org/exploratorium/ips_trackjs).

## Required packages and files

The example script uses the following files and R packages.

### User Specified, Input Files
These files specify the galleries and their boundaries:

+ [EXPLO/EXPLO_settings.csv](../EXPLO/EXPLO_settings.csv) - specifies the <*areas_Names_file*> and <*areas_Shape_file*> files for the project. See [<*projectName*>_settings](../settings.md) for a description of the variables.  
+ [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv) - lists the names of the galleries (or other areas of interest) and their colors for any visualization to be generated. See [<*areas_Names_file*>](../areas_Names_file.md) for a description of the columns.
+ [EXPLO/EXPLO_galleryShapes.csv](../EXPLO/EXPLO_galleryShapes.csv) - specifies the long/lat vertices of the galleries listed in [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv). See [<*areas_Shape_file*>](../areas_Shape_file.md) for a description of the columns.

The data collected by the IPS are saved in csv files, with each csv file containing the timestamped tracking data for each visitor as s/he moved through the museum:

+ e.g., [EXPLO/dataLogs/Test_02/LocData_V1.csv](../EXPLO/dataLogs/Test_02/LocData_V1.csv) - is the data collected by the IPS device. See [<*device.datafile*>](../device_datafile.md) for a description of the columns.

We list the IPS device data files to process and analyze in:

+ [EXPLO/dataLogs/fileList_02.csv](../EXPLO/dataLogs/fileList_02.csv) - lists the IPS data files to process and accompanying information (e.g., the device used to collect the timestamped long/lat data, the subfolder the data file is in, etc.). See [<*csv_fileList*>](../csv_fileList.md) for a description of its columns.

### R packages
+	sp
+	RgoogleMaps
+	rgdal

```
# R code to load the needed packages
# check to make sure all the packages needed are installed
required.packages = c("RgoogleMaps","rgdal", "sp")
new.packages = required.packages[!(required.packages %in% 
	installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
```

### R files in this repository

+	[R_scripts/IPS_genFunctions.R](../R_scripts/IPS_genFunctions.R)
+	[R_scripts/plot_maps.R](../R_scripts/plot_maps.R)


```
# set the working directory
setwd("..")

# load the necessary functions
source("R_scripts/IPS_genFunctions.R")
source("R_scripts/plot_maps.R")
```
## Spatial Analysis Steps
### A. Find the gallery location for each timestamped long/lat coordinate 
The following maps the long/lat coordinates collected by the IPS to mutually exclusive galleries, or areas of interest. 

```
# "EXPLO" contains scrubbed (and altered) location data from the 
# Exploratorium and is for illustrative purposes only.
projectName = "EXPLO"

# set csv_fileList, which lists the IPS log data files to process 
csv_fileList =  "fileList_02.csv"

# create a dataframe from csv_fileList
csv_df = read.csv(paste(projectName,"/dataLogs/",csv_fileList, sep=""), 
                  header = TRUE, stringsAsFactors = FALSE)
csv_df = csv_df[!(is.na(csv_df$track_id) ) & csv_df$track_id != "",]

# The following analyses assumes the time unit is seconds.  
# If the time data were collected with another time unit,
#  unitsPerSec will be used to scale the time to seconds.
unitsPerSec = 1000  #ms in a second --- the example device logs in ms

# for each IPS data file, find the area each lat/long coordinate was in 
for (i in 1:nrow(csv_df)){
  createFile_areaLocated(projectName,csv_df[i,], unitsPerSec, file_suffix = "proc")
}
```
This **outputs** a set of _proc.csv files in the same folder as the IPS device data files: 

+ e.g. [EXPLO/dataLogs/Test_02/V6_proc.csv](../EXPLO/dataLogs/Test_02/V6_proc.csv)
See [<*track\_id*>_proc](../track_id_proc.md) for a description of the columns in this file.

### B. Plot each visitor's trajectory 
This visualizes each tracked visitor's path through the museum.

```
# create the map from long/lat data in each device log file
map = getGMap(projectName, csv_df, file_suffix= "proc")

# control what is plotted 
# plot the latitude longitude coordinate points?
plotLatLong_p = TRUE   
# plot the boundaries of the areas (e.g. the galleries)?
plotAreaBoundary_p = TRUE 
# plot the path as a line?
plotPath_p = TRUE 
# mark the starting point and the end points, and 
# the position 1, 2, 3 quarters way through?
plotMarkers_p = TRUE 

# generate the plots for all the files specified in csv_fileList
for (i in 1:nrow(csv_df)){
  plot_track(projectName,   csv_df[i,], map, file_suffix= "proc",  
             plotLatLong_p ,
             plotAreaBoundary_p ,
             plotPath_p ,
             plotMarkers_p)
}

```
This **outputs** a set of png files, one for each visitor tracked: 

+ e.g. [EXPLO/results/mapping/pic/V6_proc_plot.png](../EXPLO/results/mapping/pic/V6_proc_plot.png)


![alt text](../EXPLO/results/mapping/pic/V6_proc_plot.png "One visitor's trajectory through the Exploratorium")

***One visitor's trajectory through the Exploratorium.*** *A dot represents each long/lat coordinate collected by the IPS.  Its color is the gallery it is in (according to the IPS). (A black dot indicates that the coordinate did not fall within any gallery boundaries.) The 0, 1, 2, 3, and 4 markers indicate where the visitor was at the start, 25%, 50%, 75%, and end of his/her museum visit, respectively.*

### C. Create a heat map
A heat map can help reveal areas of high and low use. 
```
# this is the time sampling rate (coordinates per ms) for the heat map.  
t_step = 10 #  samples coordinates recorded every 10 sec

# use pt_size to control the size of the dots
pt_size = 2.5

# plot the heat map for all the trajectories specified in csv_df
plot_heatMap(projectName, csv_fileList, t_step, file_suffix= "proc", 
	pt_size)

```
This **outputs** a set of png files, one for each visitor tracked: 

+ e.g. [EXPLO/results/mapping/pic/fileList_02_heatPlot.png](../EXPLO/results/mapping/pic/fileList_02_heatPlot.png)


![alt text](../EXPLO/results/mapping/pic/fileList_02_heatPlot.png "Heat Map")

***Heat map.*** *This was generated for the IPS device data files listed in the csv_fileList = fileList_01.csv.  The IPS data was subsampled at 10 seconds.  The more saturated the color, the more utilized that area was.*

## File Directory Structure
The directory structure  of the files required and generated by this script is shown below.  In this example 

+	<*projectName*> = EXPLO
+	<*areas_Shape_file*> = EXPLO_galleryShapes
+	<*areas_Names_file*> = EXPLO_galleryNames
+	<*device.datafile*> = LocData_V1
+	<*csv_fileList*> = fileList_02
+	<*subfolder*> = Test_02
+	<*track_id*> = V6


![alt text](directory_mapping.png  "File Directory Structure - Cluster-Level Validation")

***File directory structure*** 