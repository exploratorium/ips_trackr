# Sequence Analysis - Example
[R_scripts/script_analysis_sequence.R](../R_scripts/script_analysis_sequence.R)

## Description
This example  explores the sequential patterns in timing and tracking data collected for visitors during their whole-museum visit at the Exploratorium.  Each visitor's trajectory through the museum is represented as a time-ordered sequence of states, where a state is defined to be the gallery the visitor was in and the duration of that state is the amount of time from when s/he entered the gallery to when s/he exited that gallery.


## Required packages and files

This repository contains functions to process and format data collected from an indoor positioning system  to sequence data  that can then be analyzed with [TraMineR](https://cran.r-project.org/web/packages/TraMineR/index.html), an R package developed to explore sequential data in the social sciences.  

### User Specified, Input Files
This example script requires the following files:

+ [EXPLO/EXPLO_settings.csv](../EXPLO/EXPLO_settings.csv) - specifies the <*areas_Names_file*> and <*areas_Shape_file*> files for the project. See [<*projectName*>_settings](../settings.md) for a description of the variables.  
+ [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv) - lists the names of the galleries (or other areas of interest) and their colors for any visualization to be generated. See [<*areas_Names_file*>](../areas_Names_file.md) for a description of the columns.
+ [EXPLO/EXPLO_galleryShapes.csv](../EXPLO/EXPLO_galleryShapes.csv) - specifies the long/lat vertices of the galleries listed in [EXPLO/EXPLO_galleryNames.csv](../EXPLO/EXPLO_galleryNames.csv). See [<*areas_Shape_file*>](../areas_Shape_file.md) for a description of the columns.
+ [EXPLO/dataLogs/fileList_01.csv](../EXPLO/dataLogs/fileList_01.csv) - lists the IPS data files to process and accompanying information (e.g., the device used to collect the timestamped long/lat data, the subfolder the data file is in, etc.). See [<*csv_fileList*>](../csv_fileList.md) for a description of the columns.

### R packages
These R packages are required by the script and can be installed from within RStudio.  Running this script will also check for these packages and install any that are missing.

+	sp
+	TraMineR
+	WeightedCluster


```
# R code to load the needed functions and packages
# check to make sure all the packages needed are installed
required.packages = c("TraMineR","WeightedCluster", "sp")
new.packages = required.packages[!(required.packages %in% 
	installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages)
require("TraMineR")
```

### R files in this repository

+	[R_scripts/IPS_genFunctions.R](../R_scripts/IPS_genFunctions.R)
+	[R_scripts/seqFunctions.R](../R_scripts/seqFunctions.R)


```
# set the working directory
setwd("..")

# load the necessary functions
source("R_scripts/IPS_genFunctions.R")
source("R_scripts/seqFunctions.R")
```
## Sequence Analysis Steps
### A. Find the gallery location for each timestamped long/lat coordinate 
The indoor positioning system in our example collects timing and tracking data as a series of timestamped longitude and latitude coordinates, which are saved in a csv file, one csv device data file per visitor tracked. The following identifies which gallery each long/lat coordinate, collected by the IPS device, was in.  

```
# "EXPLO" contains scrubbed (and altered) location data from the Exploratorium, 
# as an example.
projectName = "EXPLO"
# set csv_fileList, which lists the IPS data files to process 
csv_fileList = "fileList_02.csv"

# create a dataframe from csv_fileList
csv_df = read.csv(paste(projectName,"/dataLogs/",csv_fileList, sep=""), 
                  header = TRUE, stringsAsFactors = FALSE)
csv_df = csv_df[!(is.na(csv_df$track_id) ) & csv_df$track_id != "",]

# The following function converts everything to seconds.  
# If the time data were collected with another time unit,
#  unitsPerSec will be used to scale the time to seconds.
unitsPerSec = 1000  #ms in a second --- the example device timestamps in ms

# for each IPS data file, find the area each long/lat coordinate was in 
for (i in 1:nrow(csv_df)){
  createFile_areaLocated(projectName,csv_df[i,], unitsPerSec, file_suffix = "proc")
}
```

This **outputs** a set of _proc.csv files in the same folder as the IPS device data files: 

+ e.g. [EXPLO/dataLogs/Test_02/V6_proc.csv](../EXPLO/dataLogs/Test_02/V6_proc.csv)
See [<*track\_id*>_proc](../track_id_proc.md) for a description of the columns in this file.


### B. Generate the sequence data for  visitors' trajectories through the museum 
The following converts the timestamped gallery location data for each visitor's trajectory to a state sequence.  The sequences are stored in State Permanence Sequence (SPS) format in the R variable seq.fn and in a csv file  that can be read in for later processing:


```
# R code to create the SPS sequence data for the 
# two example IPS datafiles found in EXPLO/dataLogs/Test_02

projectName = "EXPLO"
# t_step is the time resolution.In this example, we are interested in
# the location of the visitors tracked every second
t_step = 1 
csv_fileList = "fileList_02.csv"
seq.fn =create_areaSeqCsv(projectName, csv_fileList, t_step, file_suffix= "proc"  )

```
This **outputs** the csv file: 

+ [EXPLO/results/sequence/fileList_02_1_seq.csv](../EXPLO/results/sequence/fileList_02_1_seq.csv).  
The name of the file is derived from the csv_fileList and the t_step used. See [<*csv_fileList*>_<*t_step*>_seq](SPS_file.md) for a description of the columns in this file.



### C. Read in a stored SPS sequence dataset 
The remainder of this example uses a sequence dataset with 103 timing and tracking sequences collected by an indoor positioning system (from the time the visitor first left the Entry to the time the visitor last entered the Entry) and processed by the create_areSeqCsv function.   These SPS format sequences are in [EXPLO/results/sequence/galTrack_list_10_seq.csv](../EXPLO/results/sequence/galTrack_list_10_seq.csv) in this repository.  This sequence was created with a t_step = 10, which means that the time resolution is 10 seconds.  

```
# R code to read SPS formatted sequence data
projectName = "EXPLO"
t_step = 10
seq.fn = "galTrack_list_10_seq.csv"
data.seq = get_Seq(projectName, seq.fn)
# create the color palette
cpal(data.seq) =get_kolorVector(data.seq, projectName)

```
### D. Find the mean time in each gallery
The following calculates and outputs a plot for the mean time visitors spent in each gallery.


```
# R code to graph mean time in each gallery

# convert to minutes for graphing
a = seqmeant(data.seq)*t_step/60
max_min = ceiling(max(a))
max_y = (5+ max_min)*60/t_step

# creates the necessary directory
dir.create(file.path(projectName, "results/sequence/pic"),  
         showWarnings = FALSE)
         
# generate plot
png(file=paste(projectName, 
               "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqmtplot.png", sep=""),width=800, height=600)
seqmtplot(data.seq, ylab = "Mean Time (minutes)",  yaxis=FALSE, 
          xlab = "Gallery", xaxis = F, withlegend=F,  cex.plot = 2, 
          ylim = c(0, max_y ), border = NA)
axis(2, at=seq(from=0, to=max_y , by=60), label = seq(from=0, 
               to=max_y , by=60)/(60/t_step), cex.axis = 1)
legend("topright", alphabet(data.seq), fill=cpal(data.seq),  
               ncol=4, cex = 1.5)
dev.off()

```
The **output** plot is saved as:  

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmtplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmtplot.png)

![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmtplot.png "Mean Dwell Time Per Gallery")

***Mean Dwell Time Per Gallery.***  *On average, the Central (medium blue) gallery had the longest dwell time.*  

###  E. Visualize where visitors are at different times during their visits
The following offers a transversal look across all the visitors for different points in time.  Note that these types of  visualization do NOT preserve longitudinal information.  That is, we cannot tell if a person who was in the West (color purple) gallery 30 minutes into her visit was in the West gallery 45 minutes into her visit.

```
# R code to generate a density plot of how many visitors 
# are in each gallery for each point in time
data.sps = read.csv(paste(projectName,  
                   "/results/sequence/", seq.fn, sep = ""),  
                   header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(data.sps[,2], from = "SPS",  
                   to = "STS", compressed = TRUE), 
                   id = data.sps[,1], missing = NA,  
                   right = "finished")

png(file=paste(projectName, "/", 
                   "results/sequence/pic/",gsub(".csv", "",seq.fn), 
                    "_seqdplot.png", sep=""),width=800, height=600)
seqdplot(data.fill.seq, xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", 
                    colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         ylab = "Fraction of Visitors",  cex.plot = 2, 
         cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"), 
                    "#FFFFFF"),
         border = NA)
dev.off() 
```

The **output** plot is saved as:  

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_seqdplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqdplot.png)


![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqdplot.png "Density plot of the fraction of visitors in each gallery according to time since start of visit")

***The fraction of visitors in each gallery according to time in museum.*** *Over 40% of visitors are in the West  (purple) gallery 15 minutes into their visit.*   



To see where the plurality of visitors is, we can plot the 'modal state' of the sequences.

```
# R code to reveal where the plurality of visitors are 
# at each point in time
png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(data.fill.seq, xtstep = 900/t_step,
          xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]",
               "", colnames(data.seq)))-1),
          xlab = "Time in Museum ", withlegend = F,
          ylab = "Fraction of Visitors",  cex.plot = 1, 
          cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"),
               "#FFFFFF"),
          border = NA)
dev.off() 
```

The **output** plot is saved as:  

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmsplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmsplot.png)

![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqmsplot.png "Most Frequent Gallery")

***Gallery with the plurality of visitors according to time in museum.*** *About 45% of the visitors are in the West (purple) gallery 15 minutes into their visit.*   



We can also look at where visitors were according to the percent of their total visit time.  This requires that we first generate the time 'normalized' data for each visitor's trajectory.

```
# R code to generate SPS formatted sequence data for 
# normalized total time

# get maximum time for each visit in the data set
dwell = seqlength(data.seq)
max_dwell =max(dwell)  # in t_step units

# create and write normalized sequence data
for (i in 1:nrow(data.sps)){
  # decompose
  a = unlist(strsplit(data.sps[i,2], "-\\("))
  a = gsub("\\(", "", a)
  a = gsub("\\)", "", a)
  b = strsplit(a, ",")
  for (j in 1:length(b)){
    b[[j]][2] = round(as.numeric(b[[j]][2]) *max_dwell/dwell[i])
  }
  # compose
  b = unlist(b)
  gallery = b[seq(1, length(b), 2)]
  gal_dwell = b[seq(2, length(b), 2)]
  data.sps[i,2] = paste(paste("(", gallery , ",", gal_dwell, ")",
          sep = ""), collapse = "-")
}
write.csv(data.sps,
          paste(projectName,"/results/sequence/","normalized","_",
                gsub(".csv", "", seq.fn),
                ".csv",  sep=""),     
          row.names=FALSE) 
          

```
The **output** SPS file is saved as: 

 + [EXPLO/results/sequence/normalized_galTrack_list_10_seq.csv](../EXPLO/results/sequence/normalized_galTrack_list_10_seq.csv)

Now, we can use the newly created file to see where people were according to the fraction of their total time in the museum.

```
# use the resulting SPS file to generate a density plot
projectName = "EXPLO"
t_step = 10
seq.fn = "normalized_galTrack_list_10_seq.csv"
data.seq = get_Seq(projectName, seq.fn)
cpal(data.seq) =get_kolorVector(data.seq, projectName)
dwell = seqlength(data.seq)
max_dwell =max(dwell)  # in t_step units

png(file=paste(projectName, "/",
              "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqdplot.png", sep=""),width=800, height=600)
seqdplot(data.seq, xtstep = round(max_dwell/5),
         xtlab =  (as.numeric(gsub("[[:punct:]]", "",
              colnames(data.seq)))-1)/(5*round(max_dwell/5)),
         xlab = "Time in Museum (Fraction of total time)", 
         ylab = "Fraction of Visitors", withlegend = F, 
              cex.plot = 1, 
         border = NA)
dev.off()
```
The **output** plot is saved as:

+ [EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqdplot.png](../EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqdplot.png)

![alt text](../EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqdplot.png "Density plot of the fraction of visitors in each gallery according to fraction of their total time in the museum")

***The fraction of visitors in each gallery according to the fraction of their total time in the museum.*** *A plurality of visitors is in the Central (medium blue) gallery halfway through their visit. More visitors are in the West  (purple) gallery earlier rather than later during their visit.* 

Likewise, a plot of the most frequent gallery can be generated.

```
# use the 'time normalized' SPS file to generate a plot of 
# where the plurality of visitors are
png(file=paste(projectName, "/",
            "results/sequence/pic/",gsub(".csv", "",seq.fn),
             "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(data.seq, xtstep = round(max_dwell/5),
          xtlab =  (as.numeric(gsub("[[:punct:]]", "",
             colnames(data.seq)))-1)/(5*round(max_dwell/5)),
          xlab = "Time in Museum (Fraction of total time)",
          ylab = "Fraction of Visitors", title = NULL,
          withlegend = F, 
          border = NA)
dev.off()  
```

The **output** plot is saved as:

+ [EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqmsplot.png](../EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqmsplot.png)

![alt text](../EXPLO/results/sequence/pic/normalized_galTrack_list_10_seq_seqmsplot.png "Most frequent gallery according to fraction of total time in museum")

***Gallery with the plurality of visitors according to fraction of total time spent in museum.*** *A plurality of visitors is in the West  (purple) gallery earlier during their visit and then venture into the Central  (medium blue) gallery.*

### F. Visualize the individual sequences
To explore the longitudinal patterns in the 103 sequences, we plot each sequence as a unit along a common timeline.  

```
# R code to visualize all 103 individual sequences 

# read in the SPS formatted sequence data (not time normalized)
projectName = "EXPLO"
t_step = 10
seq.fn = "galTrack_list_10_seq.csv"
data.seq = get_Seq(projectName, seq.fn)
cpal(data.seq) =get_kolorVector(data.seq, projectName)

data.sps = read.csv(paste(projectName,  
                          "/results/sequence/", seq.fn, sep = ""),  
                    header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(data.sps[,2], from = "SPS",  
                                 to = "STS", compressed = TRUE), 
                       id = data.sps[,1], missing = NA,  
                       right = "finished")

# plot and stack each visitor's sequence and sort according to total visit time
png(file=paste(projectName, "/",
               "results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_seqIplot.png", sep=""),width=800, height=600)
seqIplot(data.seq,   xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "",
                                                  colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         cex.plot = 1, yaxis = F, sortv =  "from.end",
         cpal =  get_kolorVector(data.seq, projectName))
dev.off() 
```

The **output** plot is saved as: 

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_seqIplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqIplot.png)


![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_seqIplot.png "All sequences")

***Sequences of galleries visited for each tracked visitor. *** *With this many sequences, it is difficult to determine any clear pattern beyond the median time spent in the museum.*

### G. Identify a typology of sequences using Optimal Matching and Cluster Analysis 
To better make sense of the longitudinal aspects of the 103 sequences, we use the Optimal Matching Algorithm to calculate the distance between every pairwise sequence.  This distance can then be used to cluster similar sequences to reveal patterns.

#### G.1. Optimal Matching 
The Optimal Matching Algorithm defines the distance between two sequences as the cost associated with transforming one sequence to the other, through insertion and deletion (indel) and substitution of one state for another for each unit of time.  It allows for the calculation of a distance matrix that quantifies the difference between every pair of sequences in a dataset.

```
# R code to create the pairwise distance (or difference) matrix using Optimal Matching.

# use the conventional indel cost of 1; indel = 1
# use the transition rate as the substitution cost.  This means that the 
# more likely gallery A follows gallery B,
# the less the cost of substituting A for B;sm = "TRATE"
distance.om = seqdist(data.seq, method = "OM", indel = 1, sm = "TRATE")

```
#### G.2. Cluster Analysis 
The distance matrix allows us to conduct a cluster analysis to group similar sequences while preserving their longitudinal characteristics.

```
# R code to define clusters of sequences 

require(WeightedCluster)
# use the ward method in the cluster analysis
clusterward = agnes(distance.om, diss = TRUE, method = "ward")

# generate the dendrogram
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_dendrogram.png", sep=""),width=800, height=600)
plot(clusterward, which.plots = 2, labels = FALSE, main = "")
dev.off() 

```
The **output** plot is saved as: 

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_dendrogram.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_dendrogram.png)

![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_dendrogram.png "Dendrogram")

***Dendrogram.*** *There appears to be two dominant clusters.*   



```
# R code to compute cluster membership.  In this example, 
# we assume there are 2 clusters.
cluster_count = 2
clusters = cutree(clusterward, k = cluster_count )
clusters =  paste("Cluster", clusters)

```
### H. Visualize and explore each cluster
We look at the properties of each cluster with similar visualizations previously used to explore the entire sequence dataset.

```
# R code to  plot the average dwell time per gallery  
a = seqmeant(data.seq)*t_step/60
max_min = ceiling(max(a))
max_y = (5+ max_min)*60/t_step
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqmtplot.png", sep=""),width=800, height=600)
seqmtplot(data.seq, group = clusters,ylab = "Mean Time (minutes)",  yaxis=FALSE, 
          xlab = "Gallery", xaxis = F, withlegend=F,  cex.plot = 2, 
          ylim = c(0, max_y ), border = NA)
dev.off() 
```

The **output** plot is saved as: 

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmtplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmtplot.png). Note that the filename of the plot is generated from the csv_fileList name, the t_step value, and the number of clusters (e.g., 2) assumed.


![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmtplot.png "Mean dwell time per gallery by cluster")

***Mean Dwell Time Per Gallery.*** *Compared to Cluster 1, visitors in Cluster 2 spend more time in the West  (purple) gallery and spend less time in the Mezzanine  (orange) and Observatory  (yellow) gallery, which are at the other, eastern end of the museum.*

- - -

For a more detailed look at what distinguishes Cluster 1 and Cluster 2, we plot the individual sequences according to cluster membership.

```
# R code to plot the sequences according to cluster membership
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqIplot.png", sep=""),width=800, height=600)
seqIplot(data.seq, group = clusters,  xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         cex.plot = 1.5, yaxis = F, sortv =  "from.end",
         border = NA)
dev.off() 
```

The **output** plot is saved as:

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqIplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqIplot.png). Note that the filename of the plot is generated from the csv_fileList name, the t_step value, and the number of clusters (e.g., 2) assumed.


![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqIplot.png "All sequences")

***Sequences of galleries visited for each tracked visitor for the two clusters.*** *Cluster 1 consists of visitors who do not spend much, if any, time in the West  (purple) gallery.  If they go to the West  (purple) gallery, they do so towards the end of their museum visit.  In contrast, the visitors in Cluster 2 visit the West  (purple) gallery early and tend to spend more time in that gallery.  Few of the visitors in Cluster 2 make it to the Mezzanine  (yellow) or Observatory  (orange) gallery, which are at the other end of the museum.*   


To look at the transversal properties of the two clusters, we generate a density and  modal sequences plot.
```
# R code to  plot density of visitors in each gallery according to time and by cluster.  
# (This is a transversal property)
data.sps = read.csv(paste(projectName, "/results/sequence/", seq.fn, sep = ""), 
                    header = TRUE, stringsAsFactors = FALSE) 
data.fill.seq = seqdef(seqformat(data.sps[,2], from = "SPS", 
                      to = "STS", compressed = TRUE), 
                       id = data.sps[,1], missing = NA, right = "finished")

png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqdplot.png", sep=""),
                      width=800, height=600)
seqdplot(data.fill.seq, group = clusters,  xtstep = 900/t_step,
         xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "",
                       colnames(data.seq)))-1),
         xlab = "Time in Museum ", withlegend = F,
         ylab = "Fraction of Visitors",  cex.plot = 1, 
         cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"), "#FFFFFF"),
         border = NA)
dev.off() 
```

The **output** plot is saved as: 

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqdplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqdplot.png). 


![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqdplot.png "All sequences")

***The fraction of visitors in each gallery according to time in museum and cluster.*** *Compared to Cluster 1, the West  (purple) gallery seems to dominate the experiences of the visitors in Cluster 2*


```
# R code to  plot the gallery with the plurality of visitors according to time.  
png(file=paste(projectName, "/","results/sequence/pic/",gsub(".csv", "",seq.fn),
               "_Cluster", cluster_count, "_seqmsplot.png", sep=""),width=800, height=600)
seqmsplot(data.fill.seq, group = clusters,xtstep = 900/t_step,
          xtlab = to_HHMM (t_step *as.numeric(gsub("[[:punct:]]", "", colnames(data.seq)))-1),
          xlab = "Time in Museum ", withlegend = F,
          ylab = "Fraction of Visitors",  cex.plot = 1, 
          cpal =  c(get_kolorVector(data.seq, projectName= "EXPLO"), 
          	"#FFFFFF"),
          border = NA)
dev.off()
``` 
The **output** plot is saved as: 

+ [EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmsplot.png](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmsplot.png). 

![alt text](../EXPLO/results/sequence/pic/galTrack_list_10_seq_Cluster2_seqmsplot.png "All sequences")

***Most frequently visited gallery by time and cluster.*** *This plot shows that the West  (purple) gallery dominates the visitors experience for Cluster 2 for the first hour of the visit.  In contrast, visitors in Cluster 1 mostly move to the Central  (medium blue) gallery within the first hour.*


### I. See if there is a relationship between cluster membership and type of day data was collected

The timing and tracking data that make up this dataset were collected during evening hours with special programming and during daytime hours on weekends. To gauge if visitors moved through the galleries differently during evening hours versus weekends, we look at the visitors' cluster membership. 

```
# R code to see if there is a relationship between cluster membership 
# and type of day data was collected
recruitDay.df = read.csv(paste(projectName,  "/demog_dt.csv", sep = ""), 
                    header = TRUE, stringsAsFactors = FALSE)

# this adds the cluster membership result to information about 
# the day when data was collected
cluster.mem = rep(0, times = nrow(recruitDay.df))
for (i in 1:length(recruitDay.df$track_id ))
  cluster.mem[i] = clusters[recruitDay.df$track_id[i] == rownames(data.seq)]
recruitDay.df = cbind(recruitDay.df, cluster.mem)

#create the contingency table for chi-square  test
contingency.tbl = table(recruitDay.df$evening, recruitDay.df$cluster.mem)
chisq.test(contingency.tbl) 
```

The contingency table generated indicates that there is no detectable difference in gallery trajectories for visitors who came to the museum during its evening hours versus during weekend days:

|   | Cluster 1 | Cluster 2  |
|---|---------|----|
| **weekend day** | 	28      | 	19	 |
| **evening** | 	32      | 	24	 |

## File Directory Structure
The directory structure  of the files required and generated by this script is shown below.  In this example 

+	<*projectName*> = EXPLO
+	<*areas_Shape_file*> = EXPLO_galleryShapes
+	<*areas_Names_file*> = EXPLO_galleryNames
+	<*device.datafile*> = LocData_V1
+	<*csv_fileList*> = fileList_02
+	<*subfolder*> = Test_02
+	<*track_id*> = V6
+	<*SPSfile*>_<*t_step*> = galTrack_list_10
+	<*n*> = 2


![alt text](directory_seq.png  "File Directory Structure - Cluster-Level Validation")

***File directory structure*** 